<?php

namespace Framework\Http\Utils;

/**
 * Redirect
 * @author root87x
 * @version 1.0.2
*/

class Redirect 
{
    /**
     * 
     * @param string $location Url
     * @param array $urlParams
     * @return type
    */
    public function go301(string $location, array $urlParams = [])
    {   
        return $this->go(301, $location, $urlParams);
    }
    
    /**
     * 
     * @param string $location
     * @param array $tags
     * @param array $urlParams
     * @return type
    */
    public function go302(string $location, array $urlParams = []) 
    {
        return $this->go(302, $location, $urlParams);
    }

    /**
     * Вперёд
     * 
     * @param type $code
     * @param type $location
    */
    private function go(int $code = 200, string $location, array $urlParams = [])
    {
        $location .= !empty($url_params) ? '?' . http_build_query($url_params) : '';

        header('Location: ' . $location, true, $code);
    }
}
