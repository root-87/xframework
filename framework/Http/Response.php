<?php

namespace Framework\Http;

/**
 * Description of Response
 *
 * @author root87x
 * @version 1.0.2
*/

class Response
{
    /**
     * Установка заголовка
     * 
     * @param string $name Название
     * @param string $type Тип данных
    */
    public function header(string $name, string $type): void
    {
        header($name . ':' . $type);
    }
    
    /**
     * Устанавливает заголовок application/json
     * Возвращает результат в виде json
     * 
     * @return string
    */
    public function json(array $content, int $status = 200): string
    {
        http_response_code($status);
        $this->header('Content-Type','application/json');
        
        return json_encode($content);
    }
}
