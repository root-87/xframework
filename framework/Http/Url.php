<?php

namespace Framework\Http;

use Framework\App;

/**
 * Description of Url
 *
 * @author root87x
*/

class Url
{
    public static function to(string $url, array $attrs = [])
    {
        return '/' . App::$locale . $url . self::buildParams($attrs);
    }

    private static function buildParams(array $params)
    {
        return !empty($params) ? '?' . http_build_query($params) : '';
    }
}
