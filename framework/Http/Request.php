<?php

namespace Framework\Http;

use Framework\Components\Exceptions\NotFound;
use Framework\Storage\Session\Session;
use Framework\Storage\Session\SessionPool;

/**
 * Description of Request
 *
 * @author root87x
 */

class Request
{
    /** @var \Bavix\Router\Router */    
    private $router;
       
    /** @var \Bavix\Router\Route */    
    private $route;

    /**
     * Установить маршрутизатор
     * 
     * @param \Bavix\Router\Router $router
     * @return void
    */
    public function setRouter(\Bavix\Router\Router $router): void
    {
        $this->router = $router;
    }
    
    /**
     * Маршрут
     * 
     * @return \Bavix\Router\Route
    */
    public function route(): \Bavix\Router\Route
    {
        return $this->route = $this->router->getCurrentRoute();
    }
    
    /**
     * Аттрибуты в маршруте
     * 
     * @return array
    */
    public function attributes(): array
    {
        return $this->route()->getAttributes();
    }
    
    /**
     * Получить аттрибут по ключу
     * 
     * @param type $key
     * @return string
     */
    
    public function attribute($key): ?string
    {
        return $this->attributes()[$key] ?? null;
    }
    
    /**
     * Получить параметр по ключу
     * 
     * @param string $key
     * @param mixed $default
     * @return mixed
    */
    public function getParam(string $key, $default = null)
    {
        $stream = $this->getParams();
        return $stream[$key] ?? $default;
    }
    
    /**
     * Получить все параметры
     * 
     * @return array
    */
    public function getParams(): array
    {
        $stream_get = filter_input_array(INPUT_GET) ?? [];
        $stream_post = filter_input_array(INPUT_POST) ?? [];
        
        $stream = $stream_get + $stream_post;

        return $stream;
    }
    
   
    /**
     * Если https протокол
     * 
     * @return type
    */
    public function isHttps()
    {
        return (isset($_REQUEST['HTTPS']) && $_SERVER['HTTPS'] != 'off');
    }
    
    /**
     * true если запрос POST
     * @return bool
    */
    
    public function isPost(): bool
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }
    
    /**
     * Eсли Ajax
     * @return bool
    */
    
    public function isAjax(): bool
    {
        return (
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest'
        );
    }
    
    /**
     * Если есть файлы
     * 
     * @return type
     */
    
    public function isFiles()
    {
        return isset($_FILES) && !empty($_FILES);
    }
    
    /**
     * Хост
     * @return type
     */
    
    public function getHost()
    {
        $protocol = $this->IsHttps() ? 'https://' : 'http://';
        return $protocol .= $_SERVER['HTTP_HOST'];
    }
    
    /**
     * Метод запроса
     * 
     * @return type
     */
    
    public function getMethod()
    {
        $method = $_SERVER['REQUEST_METHOD'];

        if($this->IsPost()) {
            if(isset($_SERVER['X-HTTP-METHOD-OVERRIDE'])) {
                $method = strtoupper($_SERVER['X-HTTP-METHOD-OVERRIDE']);
            }
        }

        return $method;
    }

    public function getPath($baseUrl = null)
    {
        $pathInfo = $_SERVER['REQUEST_URI'];

        if (!$pathInfo) {
            $pathInfo = '/';
        }

        $schemeAndHttpHost = $this->getHost();

        if (strpos($pathInfo, $schemeAndHttpHost) === 0) {
            $pathInfo = substr($pathInfo, strlen($schemeAndHttpHost));
        }

        if ($pos = strpos($pathInfo, '?')) {
            $pathInfo = substr($pathInfo, 0, $pos);
        }

        if (null !== $baseUrl) {
            $pathInfo = substr($pathInfo, strlen($pathInfo));
        }

        if (!$pathInfo) {
            $pathInfo = '/';
        }

        return $pathInfo;
    }

    /**
     * Редирект
     * @return Helper\Redirect
    */
    
    public function redirect(): Utils\Redirect
    {
        return new Utils\Redirect();
    }
    
    /**
     * Запомнить url
     * 
     * @param string $name
     * @param string $path
    */
    public function memoryUrl(string $name, string $path)
    {
        $session = new Session($name);
        $session->set(urlencode($path));
    }
    
    /**
     * Восстановить урл
     * @param string $name
     * 
     * @return string|null
    */
    public function recvMemoryUrl(string $name)
    {
        $session = new SessionPool(new Session($name));

        if (!$session->has()) {
            return null;
        }
        
        $url = $session->get();
        $session->delete();
        
        return urldecode($url);
    }

}