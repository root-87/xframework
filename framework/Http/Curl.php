<?php

namespace Framework\Http;

/**
 * Description of Curl
 *
 * @author root87x
 */
class Curl 
{
    private $curl;
    
    public function __construct(string $url, array $options = []) 
    {
        $this->curl = curl_init($url);
        curl_setopt_array($this->curl, $options);
    }
    
    public function request()
    {
        $response = curl_exec($this->curl);
        
        $http_code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        $error = curl_error($this->curl);
        
        curl_close($this->curl);
        
        if ($response['http_code'] !== 200) {
            http_response_code($response['http_code']);
        }
        
        return [
            'response' => $response,
            'http_code' => $http_code,
            'error' => $error
        ];
    }
}
