<?php

namespace Framework\Psr;

use \Framework\Http\{
    Request,
    Response
};

/**
 * Интерфейс app контроллера
 * 
 * @author root87x
 * @version 1.0.1
*/
interface Controller
{
    public function view(Request $request, Response $response);
}
