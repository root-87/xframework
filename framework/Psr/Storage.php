<?php

namespace Framework\Psr;

/**
 * Интерфейс хранилище
 * 
 * @author root87x
 * @version 1.0.0
*/
interface Storage
{
    public function add(string $key, $value);
    public function get(string $key);
    public function has(string $key);
    public function delete(string $key);
    public function count();
}
