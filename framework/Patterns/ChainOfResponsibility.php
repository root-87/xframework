<?php

namespace Framework\Patterns;

use Framework\Http\{
    Response, 
    Request    
};

abstract
class ChainOfResponsibility 
{
    /** @var ChainIfResponsibility */
    private $successor;
    
    public function __construct(ChainOfResponsibility $successor = null)
    {
        $this->successor = $successor;
    }
    
    public function handler(Request $request, Response $response)
    {
        $successor = $this->proccessing($request, $response);
        
        if ($successor === null) {
            
            if ($this->successor !== null) {
                $successor = $this->successor->handler($request, $response);
            }
            
        }
        
        return $successor;
    }
    
    abstract protected function processing(Request $request, Response $response);
}