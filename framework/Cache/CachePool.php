<?php

namespace Framework\Cache;

use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;

/**
 * Description of CachePool
 *
 * @author root87x
 * @version 1.0.1
 */
abstract 
class CachePool implements CacheItemPoolInterface
{
    /**
     * Получить кеш по ключу
     * 
     * @param string $key
     * @return CacheItemInterface
    */
    public function getItem($key): CacheItemInterface
    {
        return $this->cache->name($key);
    }
    
    /**
     * Получить коллекцию по ключам
     * 
     * @param array $keys
     * @return array|null
    */
    public function getItems(array $keys = array())
    {
        if (empty($keys)) {
            return null;
        }
        
        $collection = [];
        $cache = $this->cache;
        
        foreach ($keys as $item) {
            clone $cache;
            $collection[$item] = $cache->name($item)->get();
        }
        
        return $collection;
    }
    
    /**
     * Проверяет содержится кеш с таким ключом
     * 
     * @param string $key
     * @return bool
    */
    public function hasItem($key)
    { 
        return !empty($this->cache->name($key)->get());
    }
    
    public function clear()
    {
        return $this->cache->handler()->flush();
    }
    
    public function deleteItem($key)
    {
        
    }
    
    public function deleteItems(array $keys)
    {
        
    }
    
    public function save(CacheItemInterface $item)
    {
        
    }
    
    public function saveDeferred(CacheItemInterface $item)
    {
        
    }
    
    public function commit()
    {
        
    }
}
