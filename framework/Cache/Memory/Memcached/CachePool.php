<?php

namespace Framework\Cache\Memory\Memcached;

use Framework\Cache\Memory\Memcached\Cache;

/**
 * Pool memory cache
 *
 * @author root87x
 * @version 1.0
*/

class CachePool extends \Framework\Cache\CachePool
{
    /** @var Cache */
    protected $cache;
    
    public function __construct(array $config = [])
    {
        $this->cache = new Cache($config);
    }
}
