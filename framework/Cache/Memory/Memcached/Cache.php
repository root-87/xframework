<?php

namespace Framework\Cache\Memory\Memcached;

use Psr\Cache\CacheItemInterface;

/**
 * Memory cache tools
 *
 * @author root87x
 * @version 1.0.3
*/

class Cache extends \Framework\Cache\Cache implements CacheItemInterface
{
    /** @var \Memcached */
    protected $handler;
    
    /** @var bool */
    protected $connect;
    
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        
        if (!class_exists('\Memcached')) {
            throw new \Exception('Memcached undefined');
        }
        
        $this->handler = new \Memcached;
        $this->connect = $this->handler->addServer($this->host, $this->port) OR die('Could not connect');
    }
    
    /**
     * Установить кеш
     * 
     * @param type $value
    */
    public function set($value): void
    {
        $expires_at = $this->expiresAfter;
        
        if ($this->expiresAt !== null) {
            $expires_at = $this->expiresAt->getTimestamp() - time();
        } 
        
        $this->state = $this->handler->set($this->name, $value, $expires_at);
    }
}
