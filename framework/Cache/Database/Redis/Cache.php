<?php

namespace Framework\Cache\Database\Redis;

use Psr\Cache\CacheItemInterface;

/**
 * Redis of Cache
 *
 * @package Redis
 * 
 * @author root87x
 * @version 1.0
 */
class Cache extends \Framework\Cache\Cache implements CacheItemInterface
{
    /** @var \Predis\Client */
    protected $handler;
    
    /** @var array */
    protected $config = [
        'host' => 'localhost',
        'port' => 6379
    ];
    
    public function __construct(array $config = [])
    {
        parent::__construct($config);      
        
        if (!class_exists('\Predis\Client')) {
            throw new \Exception('Please install Predis\Client package');
        }
        
        $this->handler = new \Predis\Client($this->config);
    }
    
    public function set($value): void
    {
        $expires_at = time() + $this->expiresAfter;
        
        if ($this->expiresAt !== null) {
            $expires_at = $this->expiresAt->getTimestamp() - time();
        } 
        
        parent::set($value);
        $this->handler->expireAt($this->name, $expires_at);
    }
}
