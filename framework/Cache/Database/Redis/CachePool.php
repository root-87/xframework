<?php

namespace Framework\Cache\Database\Redis;

use Framework\Cache\Database\Redis\Cache;

/**
 * Redis of CachePool
 *
 * @author root87x
 * @version 1.0
 */
class CachePool extends \Framework\Cache\CachePool
{
    /** @var Cache */
    protected $cache;
    
    public function __construct()
    {
        $this->cache = new Cache();
    }
}
