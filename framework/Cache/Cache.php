<?php

namespace Framework\Cache;

/**
 * Description of Cache
 *
 * @author root87x
 */
abstract 
class Cache
{
    protected $handler;
    
    /** @var bool */
    protected $state = false;
    
    /** @var string */
    protected $name = null;
    
    /** @var \DateTime время жизни */
    protected $expiresAt;
    
    /** @var int время жизни в секундах */
    protected $expiresAfter = 1120;

    /** @var array */
    protected $config = [
        'host' => 'localhost',
        'port' => 11211
    ];
    
    public function __construct(array $config = [])
    {
        $this->config = array_merge($this->config, $config);
    }
    
    public function __get(string $key) 
    {
        return $this->config[$key] ?? null;
    }
    
    /**
     * Возвращает handler
     * 
     * @return \Memcache
    */
    public function handler()
    {
        return $this->handler;
    }
    
    /**
     * Возвращает состояние кеша
     * @return bool
    */
    public function isHit(): bool
    {
        return $this->state;
    }
    
    /**
     * Получить название кеша
     * 
     * @return string
    */
    public function getKey(): string 
    {
        return $this->name;
    }
    
    /**
     * Установить название кеша
     * 
     * @param string $name
    */
    public function name(string $name): self
    {
        $this->name = $name;
        return $this;
    }
    
    /**
     * Установить точную дату и время истечения срока действия
     * 
     * @param \DateTime $expiration
    */
    public function expiresAt($expiration): void
    {
        $this->expiresAt = $expiration;
    }
    
    /**
     * Установка времени истечения кеша в секундах
     * @param int $time
    */
    public function expiresAfter($time): void
    {
        $this->expiresAfter = $time;
    }
    
    /**
     * Установить кеш
     * 
     * @param type $value
    */
    public function set($value): void
    {
        $this->handler->set($this->name, $value);
    }
    
    /**
     * Получить кеш
     * 
     * @return string
    */    
    public function get()
    {
        return $this->handler->get($this->name);
    }
    
}
