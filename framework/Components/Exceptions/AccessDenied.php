<?php

namespace Framework\Components\Exceptions;

/**
 * Exception of Access denied
 *
 * @author root87x
*/

class AccessDenied extends Exception
{
    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
