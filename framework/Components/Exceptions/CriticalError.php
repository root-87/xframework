<?php

namespace Framework\Components\Exceptions;

/**
 * Description of CriticalError
 *
 * @author root87x
 */
class CriticalError extends Exception 
{
    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null) 
    {
        parent::__construct($message, $code, $previous);
    }
}
