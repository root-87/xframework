<?php

namespace Framework\Components\Exceptions;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Description of NotFoundException
 *
 * @author root87x
*/

class NotFound extends Exception 
{
    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null) 
    {
        http_response_code(404);
        parent::__construct($message, $code, $previous);
    }

}
