<?php

namespace Framework\Components\Exceptions;

use Framework\App;
use Framework\Views\ViewAdapter;
use Framework\Views\Templates\Twig;

/**
 * Main exception
 *
 * @author root87x
 * @version 1.1.1
*/

abstract 
class Exception extends \Exception {
    
    /** @var ViewAdapter */
    protected $view;   
    
    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null) 
    {
        parent::__construct($message, $code, $previous);
        
        $di = App::DI()->get('exception');
        
        $this->view = $di->getView();
        $this->view->setViewPath($di::view . '/' . class_basename(static::class));
        $this->view->addVar('i18n', $di->getTranslater());
        
    }

    public function view()
    {
        return $this->view->render('web', [
            'message' => $this->message
        ]);
    }
}
