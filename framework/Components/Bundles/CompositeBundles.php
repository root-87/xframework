<?php

namespace Framework\Components\Bundles;

/**
 * Description of Bundles
 *
 * @author root87x
*/

abstract 
class CompositeBundles
{
    protected 
        $name,    
        $storage;
    
    public function __construct($name, $storage) 
    {
        $this->name = $name;
        $this->storage = $storage;
    } 
    
    public function __get($key) 
    {
        return $this->storage[$key] ?? null;
    }
    
    public function has($key) 
    {
        return isset($this->storage[$key]);
    }
}
