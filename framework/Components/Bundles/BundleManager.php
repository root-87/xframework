<?php

namespace Framework\Components\Bundles;

/**
 * Description of BundleManager
 *
 * @author root87x
*/

class BundleManager 
{
    private $storage = [];
    
    public function __get($name) {
        return $this->get($name);
    }
    
    public function set(array $storage)
    {
        foreach ($storage as $classname) {
            $init = new \ReflectionClass($classname);
            $bundle_name = strtolower($init->getShortName());
            $this->storage[$bundle_name] = $init->newInstance();
        }
    }
    
    public function get(string $name)
    {
        return $this->storage[$name] ?? null;
    }
    
}
