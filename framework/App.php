<?php

namespace Framework;

use Framework\Http\Request;
use Framework\Storage\DIContainer;
use Framework\Config\Psr\Config;

/**
 * Description of Application
 *
 * @author root87x
*/

class App
{   
    /** @var Локализация приложения */  
    public static $locale = 'ru';

    /** @var \Src\App */
    private static $instance;
    
    /** @var Request */
    private $request;

    /** @var IConfig */   
    private $config;
    
    /** @var DIContainer **/
    private $container;
            
    public function __construct(Config $config) 
    {
        self::$instance = $this;
        
        $this->container = new DIContainer();
        
        foreach ($config->get('app.dicontainer') as $name => $namespace) {
            $this->container->set($name, $namespace);
        }

        $this->config = $config;
    }
    
    public function __call(string $action, array $params) 
    {
        [$controller, $args] = $params;

        $methods = array_flip(get_class_methods($controller));

        if (isset($methods['beforeAction'])) {
            $controller->{'beforeAction'}();
        }
                
        if (isset($methods['actions'])) {
            $actions = $controller->{'actions'}($this->request);
            if (isset($actions[$action])) {
                $actions[$action]();
            }
        }
        
        $ret = $controller->{$action}($this->request, new Http\Response());

        if (isset($methods['afterAction'])) {
            $controller->{'afterAction'}($ret);
        }
        
        return (string) $ret;
    }
    
    /**
     * Call method from controller
     * 
     * @return mixed
     * @throws NotFound
    */
    public function __toString()
    {
        $attribute = $this->request->attributes();
        $middleware = $attribute['middleware'] ?? null;
        
        $controller = Str::ucwords(ucfirst($attribute['controller']) . 'Controller', '-');
        $location = $attribute['location'];
        
        $namespace = $location . '\\' . $controller;   
                
        if ($middleware !== null) {
            if (is_array($middleware)) {
                foreach ($middleware as $call) {
                    $mware = (new $call)($this->request, new Http\Response());
                }
            } else {
                $mware = (new $middleware)($this->request, new Http\Response());
            }
        }

        return $this->{$attribute['action']}(new $namespace($this->container), $attribute);
    }
    
    /**
     * self
     * 
     * @return App;
    */
    public static function I(): App
    {
        return self::$instance;
    }
        
    /**
     * Доступ к конфигам
     * 
     * @param string $path
     * @return type
    */
    public function conf(string $path)
    {
        return $this->config->get($path);
    }
    
    /**
     * Установить локаль
     * 
     * @param string $locale
    */
    public static function setLocale(string $locale): void
    {
        static::$locale = $locale;
    }
    
    /**
     * Pull request
     * @param Request $request
    */
    public function setRequest(Request $request): void
    {
        $this->request = $request;
    }
    
    /**
     * Request
     * @return Request
    */
    public function getRequest(): Request
    {
        return $this->request;
    }
    
    /**
     * 
     * @return DIContainer
    */
    public function getContainer()
    {
        return $this->container;
    }
}
