<?php

namespace Framework;

/**
 * Description of String
 *
 * @author root87x
 */
class Str
{
    
    public static function ucwords(string $str, $delimiter = '-')
    {
        return str_replace($delimiter, '', ucwords($str, $delimiter));
    }
    
    /**
     * Генерирует случайную строку
     * 
     * @param type $length - длина строки
     * @return string
    */
    
    public static function genString(int $length = 12): string
    {
        $chars = '123456789QqWwEeRrTtYyUuIiOoPpaAsSdDFfGgHhJjKkLlZzXxCcVvBbNnMm';
        $string = '';

        while($length--) {
            $string .= $chars[mt_rand(0, strlen($chars) - 1)];
        }

        return $string;
    }
}
