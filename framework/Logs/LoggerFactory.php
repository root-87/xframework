<?php

namespace Framework\Logs;

use Framework\Logs\Parts\{
    DBLogger,
    FileLogger
};

/**
 * Сохранение логов в файл
 *
 * @author root87x
 */
class LoggerFactory
{
    public static function DBLogger()
    {
        //todo code
    }
    
    /**
     * Файловый логгер
     * @return FileLogger
    */
    public static function FileLogger(): FileLogger
    {
        return new FileLogger();
    }
}
