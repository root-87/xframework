<?php

namespace Framework\Logs;

use Framework\Logs\Model\IlluminateModel;

/**
 * Description of DatabaseLogger
 *
 * @author root87x
 */
class DBLogger implements Psr\Logger 
{    
    /**
     * GET
     * 
     * @return IlluminateModel
    */
    public function getLogger()
    {
        return (new IlluminateModel($this->model));
    }
}
