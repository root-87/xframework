<?php

namespace Framework\Logs\Parts;

/**
 * Логгер
 *
 * @author root87x
 * @version 1.0.2
*/

class FileLogger implements 
    \Framework\Logs\Psr\Logger, 
    \Framework\Logs\Psr\FileLogger
{
    /** @var string */
    private $name;
    
    /** @var string */
    private $path;

    /** @var int */
    private $state = 100;
    
    /** @var string */
    private static $ext = 'log';
    
    /**
     * 
     * @param string $ext
     * @return void
    */
    public function setExtension(string $ext): void
    {
        self::$ext = $ext;
    }
    
    /**
     * 
     * @param string $name
     * @return void
    */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
    
    /**
     * 
     * @param string $path
     * @return void
    */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }
    
    /**
     * 
     * @param int $state
     * @return void
    */
    public function setState(int $state): void
    {
        $this->state = $state;
    }
    
    /**
     * Эксземпляр класса \Monolog\Logger
     * @return \Monolog\Logger
    */
    
    public function getLogger(): \Monolog\Logger
    {
        $logger = new \Monolog\Logger($this->name);
        
        if (!file_exists($this->path) && !mkdir($this->path, 0777, true)) {
            throw new \Exception('Cannot create dir to ' . $this->path);
        }
        
        $fullpath = $this->path . '/' . $this->name . '.' . self::$ext;
        
        $logger->pushHandler(new \Monolog\Handler\StreamHandler($fullpath, $this->state));
        
        return $logger;
    }
}
