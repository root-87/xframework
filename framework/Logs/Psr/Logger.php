<?php

namespace Framework\Logs\Psr;

/**
 * Description of Logger
 *
 * @author root87x
 * @version 1.0
 */
interface Logger
{
    public function getLogger();
}
