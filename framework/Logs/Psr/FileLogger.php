<?php

namespace Framework\Logs\Psr;

/**
 * Интерфейс для Файлового логгера
 * 
 * @author root87x
 * @version 1.0.0
 */
interface FileLogger
{
    public function setName(string $name): void;
    
    public function setPath(string $path): void;
    
    public function setState(int $state): void;
}
