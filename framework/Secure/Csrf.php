<?php

namespace Framework\Secure;

use Framework\Str;
use Framework\Storage\Session\{
    Session,
    SessionPool
};
use Framework\Components\Exceptions\CriticalError;

/**
 * Description of Csrf
 *
 * @author root87x
*/

trait Csrf
{
    /**
     * Генерация токена
     * 
     * @return string
     * @throws \Exception
    */
    
    public function csrfoken(): string
    {
        $csrf_token = base64_encode(Str::genString(15));
        
        $set = new Session('token');
        $set->set($csrf_token);
        
        $session = new SessionPool($set);
        
        if (!$session->has()) {
            throw new CriticalError('Ошибка при генерации токена');
        }
        
        return $csrf_token;
    }
    
    /**
     * Получить действующий токен
     * @return string|null
    */
    public function getCsrfToken(): ?string
    {
        $token = new SessionPool(new Session('token'));
        $token_value = $token->get();

        $token->delete();
        
        return $token_value ?? null;
    }
    
    /**
     * Валидация токена
     * 
     * @param string $token - токен который будет проверяться
     * @return boolean
     * @throws \Exception
    */
    public function checkCsrfToken(string $token): bool
    {
        return $this->getCsrfToken() === $token;    
    }
}
