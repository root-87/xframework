<?php

namespace Framework\Secure\Access\Rbac;

use PhpRbac\Rbac;

/**
 * Класс для работы с контролем доступа
 *
 * @author root87x
 * @version 1.0.3
*/

class ControlAccess 
{
    /** @var Rbac */
    private $rbac;
    
    public function __construct() 
    {
        $this->rbac = new Rbac();
    }
    
    /**
     * Возвращает результат по правилу
     * 
     * @param string $path Access path
     * @param int $userId User id
     * @return bool
     */
    
    public function check(string $path, int $userId): bool
    {
        if ((bool)$this->rbac->Permissions->returnId($path)) {
            return $this->rbac->check($path, $userId);
        }
        
        return false;
    }
}
