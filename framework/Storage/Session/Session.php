<?php

namespace Framework\Storage\Session;

/**
 * Класс для работы с сессиями
 *
 * @author root87x <developer-home@yandex.ru>
 * 
 * @version 1.0.2
 * @package Storage
 * @subpackage Session
*/

class Session
{
    /**
     * Ключ сесии
     * 
     * @access private
     * @var string
    */
    private $key = null;
    
    public function __construct(string $key)
    {
        if (!session_id()) {
            session_start();
        }
        
        $this->key = $key;
    }
    
    public function __get($key)
    {
        return $this->getKey($key); 
    }
    
    /**
     * установить сессию
     * 
     * @access public
     * @param string $value Значение
     * @return bool
    */
    public function set(string $value): bool
    {
        return $_SESSION[$this->key] = $value;
    }
    
    /**
     * Получить индекс сессии
     * 
     * @param string $key
     * @return string
    */
    public function getKey(string $key): string
    {
        return $this->key;
    }
}
