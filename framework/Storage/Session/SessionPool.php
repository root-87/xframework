<?php

namespace Framework\Storage\Session;

/**
 * Класс для работы с существующей сессией
 * 
 * @author root87x <developer-home@yandex.ru>
 * 
 * @version 1.0.2
 * @package Storage
 * @subpackage Session
*/

class SessionPool
{
    /**
     * Свойство хранит экземпляр класса Session
     * 
     * @access private
     * @var Session 
    */
    private $session;
    
    public function __construct(Session $session) 
    {
        $this->session = $session;
    }
    
    /**
     * Возвращает индентификатор сессии
     * 
     * @access public
     * @return string
    */
    public function id(): string
    {
        return session_id();
    }
    
    /**
     * Возвращает занчение
     * 
     * @access public
     * @return null|string
    */
    public function get()
    {
        return $_SESSION[$this->session->key] ?? null;
    }

    /**
     * Проверка на существование
     * 
     * @access public
     * @return bool
    */
    public function has(): bool
    {
        return isset($_SESSION[$this->session->key]);
    }
    
    /**
     * Удаление
     * 
     * @access public
     * @return bool
    */
    public function delete(): bool
    {
        unset($_SESSION[$this->session->key]);
        return $this->has();
    }
}
