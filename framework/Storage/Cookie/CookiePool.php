<?php

namespace Framework\Storage\Cookie;

/**
 * Description of Cookies
 *
 * @author root87x
*/

class CookiePool
{
    private $cookie;  

    public function __construct(Cookie $cookie) 
    {
        $this->cookie = $cookie;
    }

    public function has(): bool
    {       
        return isset($_COOKIE[$this->cookie->key]) ?? null;
    }
    
    final public function get() 
    {
        return $_COOKIE[$this->cookie->key] ?? null;
    }
    
    final public function delete(): bool
    {
        if ($this->has()) {
            $cookie = new Cookie($this->cookie->key, time() - 1000000);
            return $cookie->set('');
        }
        
        return false;
    }
}
