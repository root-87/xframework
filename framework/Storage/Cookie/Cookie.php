<?php

namespace Framework\Storage\Cookie;

/**
 * Description of Cookie
 *
 * @author root87x
*/

class Cookie
{
    public $key;
    
    private $expireAt;
    private $path = '/';
    private $domain;
    private $secure = false;
    private $httponly = true;
    
    public function __construct(string $key, $expire = 3600 * 6)
    {
        $this->key = $key;
        $this->expireAt = time() + $expire;
    }
    
    public function secure(bool $value): void
    {
        $this->secure = $value;
    }
    
    public function path(string $value): void
    {
        $this->path = $value;
    }
    
    public function domain(string $value): void
    {
        $this->domain = $value;
    }
    
    public function httpOnly(bool $value): void
    {
        $this->httponly = $value;
    }
    
    public function expireAt($value) 
    {
        $this->expireAt = $value;
    }
    
    final public function set(string $value): bool
    {
        return setcookie(
            $this->key, 
            $value, 
            $this->expireAt, 
            $this->path, 
            $this->domain, 
            $this->secure, 
            $this->httponly
        );
    }
    
}
