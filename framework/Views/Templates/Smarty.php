<?php

namespace Framework\Views\Templates; 

use Smarty AS SmartyMaster;

/**
 * Description of Smarty
 *
 * @author root87x
 * @version 1.0.1
*/

class Smarty implements \Framework\Views\Psr\View 
{
    /** @var SmartyMaster Handler */
    private $view;
    
    /** @var string Путь к шаблону */
    private $viewPath;

    private $layoutPath;
    
    /** @var array Переменные */
    private $vars = [];
    
    /** @var string Расширение шаблона */
    private $extension = 'tpl';
    
    
    /**
     * Конструктор
     * @param SmartyMaster $view Smarty
    */
    
    public function __construct()
    {
        $this->view = new SmartyMaster();
    }
    
    /**
     * Получить массив переменных
     * 
     * @return array
    */
    public function getVars()
    {
        return $this->vars;
    }
    
    /**
     * Получить переменную по ключу
     * 
     * @param string $key
     * @return mixed
    */
    public function getVar(string $key)
    {
        return $this->vars[$key] ?? null;
    }
    
    /**
     * Расширение файла
    */
    public function setExtension(string $ext)
    {
        $this->extension = $ext;
    }
    
    /**
     * 
     * @param string $path
    */
    public function setViewPath(string $path) 
    {
        $this->viewPath = $path;
    }
    
    /**
     * @param string $path
    */
    public function setLayoutPath(string $path) 
    {
        $this->layoutPath = $path;
    }
    
    /**
     * Добавить переменную в smarty
     * 
     * @param string $name Ключ
     * @param type $value Значение
    */
    public function addVar(string $name, $value) 
    {
        $this->view->assign($name, $value);
    }
    
    /**
     * Добавить несколько и более переменных в smarty
     * @param array $vars
    */
    public function addVars(array $vars)
    {
        foreach ($vars as $name => $value)
        {
            $this->view->assign($name, $value);
        }
    }
    
    /**
     * Зарегистрировать объект
     * 
     * @param string $name
     * @param type $obj
    */
    public function registerObject(string $name, $obj)
    {
        $this->view->registerObject($name, $obj);
    }
    
    /**
     * Регистрирует класс для smarty шаблонизатора
     * @param string $name - Имя
     * @param string $namespace - Ппуть до класса
     */
    
    public function registerClass(string $name, string $namespace)
    {
        $this->view->registerClass($name, $namespace);
    }
    
    /**
     * Вывод на экран
     * 
     * @param string $view Название шаблона
     * @param array $data Данные
     * @param type $controller Контроллер вызова
     * @return mixed
     * @throws \Exception
     */
    
    public function render(string $view, array $data = array()) 
    {
        $filepath = $this->viewPath . '/'  . $view . '.' . $this->extension;
    
        if(!file_exists($filepath)) {
            throw new \Exception('Шаблон ' . $filepath . ' не найден');
        }

        $this->addVar('layoutPath', $this->layoutPath);
        $this->addVar('viewPath', $this->viewPath);

        $this->view->assign($data);
                
        return $this->view->display($filepath);
    }
    
    public function viewPartial(string $pathView, array $data = array()) 
    {
        if(!file_exists($pathView)) {
            throw new \Exception('Шаблон ' . $pathView . ' не найден');
        }

        $this->view->assign($data);
                
        return $this->view->display($pathView);
    }
}
