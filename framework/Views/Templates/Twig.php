<?php

namespace Framework\Views\Templates;


/**
 * Шаблонизатор Twig
 *
 * @author root87x
 * @version 1.0.1
*/

class Twig implements \Framework\Views\Psr\View
{
    /** @var \Twig_Environment Handler */
    private $view;
    
    private $loader;
    
    /** @var array Переменные */
    private $vars = [];
    
    /** @var string Расширение шаблона */
    private $extension = 'twig';
    
    public function __construct()
    {
        $this->loader = new \Twig_Loader_Filesystem();
        $this->view = new \Twig_Environment($this->loader, [
            'debug' => true
        ]);
        $this->view->addExtension(new \Twig_Extension_Debug());
    }
    
    /**
     * Получить массив переменных
     * 
     * @return array
    */
    public function getVars()
    {
        return $this->vars;
    }
    
    /**
     * Получить переменную по ключу
     * 
     * @param string $key
     * @return mixed
    */
    public function getVar(string $key)
    {
        return $this->vars[$key] ?? null;
    }
    
    /**
     * Расширение файла
    */
    public function setExtension(string $ext)
    {
        $this->extension = $ext;
    }
    
    /**
     * 
     * @param string $path
    */
    public function setCache(string $path) {
        $this->view->setCache($path);
    }
    
    /**
     * 
     * @param string $path
    */
    public function setViewPath(string $path) 
    {
        $this->loader->setPaths($path);
    }
    
    /**
     * @param string $path
    */
    public function setLayoutPath(string $path) 
    {
        $this->loader->setPaths($path, 'layout');
    }
    
    /**
     * Добавить переменную в smarty
     * 
     * @param string $name Ключ
     * @param type $value Значение
    */
    public function addVar(string $name, $value) 
    {
        $this->vars[$name] = $value; 
    }
    
    /**
     * Добавить несколько и более переменных в smarty
     * @param array $vars
    */
    public function addVars(array $vars)
    {
        foreach ($vars as $name => $value)
        {
            $this->vars[$name] = $value;
        }
    }
    
    /**
     * Зарегистрировать объект
     * 
     * @param string $name
     * @param type $obj
    */
    public function registerObject(string $name, $obj)
    {
        $this->addVar($name, new $obj());
    }
    
    /**
     * Регистрирует класс для smarty шаблонизатора
     * @param string $name - Имя
     * @param string $namespace - Ппуть до класса
     */
    
    public function registerClass(string $name, string $namespace)
    {
        $this->addVar($name, $namespace);
    }
    
    /**
     * Вывод на экран
     * 
     * @param string $view Название шаблона
     * @param array $data Данные
     * @param type $controller Контроллер вызова
     * @return mixed
     * @throws \Exception
    */
    public function render(string $view, array $data = []) 
    {     
        $template = $this->view->loadTemplate($view . '.' . $this->extension);
        return $template->render(array_merge($this->vars, $data));
    }
    
    public function viewPartial(string $view, array $data = []) 
    {
        return $this->render($view, $data);
    }
}
