<?php

namespace Framework\Views;

/**
 * Description of View
 *
 * @author root87x
*/

class ViewAdapter implements \Framework\Views\Psr\View 
{    
    /* @var $view Framework\Views\IView */
    private $view;
    
    /**
     * view adapter
     * @param \Framework\Views\Psr\View $view
     */
    
    public function __construct(\Framework\Views\Psr\View $view) 
    {
        $this->view = $view;        
    }
    
    /**
     * Получить массив переменных
     * 
     * @return array
    */
    public function getVars()
    {
        return $this->view->getVars();
    }
    
    /**
     * Получить переменную по ключу
     * 
     * @param string $key
     * @return type
    */
    public function getVar(string $key)
    {
        return $this->view->getVar($key);
    }
    
     /**
     * 
     * @param string $path
    */
    public function setCache(string $path) {
        $this->view->setCache($path);
    }
    
    /** 
     * Установка расширения файла
     * 
     * @param string $ext
    */
    public function setExtension(string $ext)
    {
        $this->view->setExtension($ext);
    }
    
    /**
     * Путь до шаблона
     * 
     * @param string $path
    */
    public function setViewPath(string $path) 
    {
        $this->view->setViewPath($path);
    }
    
    /**
     * Директория с шаблонами
     * 
     * @param string $path
    */
    public function setLayoutPath(string $path)
    {
        $this->view->setLayoutPath($path);
    }
    
    /**
     * Регистрация объекта
     * 
     * @param string $name
     * @param type $obj
    */
    public function registerObject(string $name, $obj)
    {
        $this->view->registerObject($name, $obj);
    }
    
    /**
     * Регистрирует класс для smarty шаблонизатора
     * 
     * @param string $name - Имя
     * @param string $namespace - Ппуть до класса
     */
    public function registerClass(string $name, string $namespace)
    {
        $this->view->registerClass($name, $namespace);
    }
    
    /**
     * Добавляет переменную в шаблон
     * 
     * @param string $name - Имя
     * @param mixed $value - Значение
    */
    public function addVar(string $name, $value)
    {
        $this->view->addVar($name, $value);
    }
    
    /**
     * Добавляет несколько и более переменных в шаблон
     * 
     * @param array $vars
    */
    public function addVars(array $vars) 
    {
        $this->view->addVars($vars);
    }
    
    /**
     * Рендер шаблона
     * 
     * @param string $view
     * @param array $data
     * @return string
    */
    public function render(string $view, array $data = [])
    {
        return $this->view->render($view, $data);
    }
    
    /**
     * Рендер отдельного шаблона
     * 
     * @param string $viewPath Полный путь до шаблона
     * @param array $data
     * @return string
    */
    public function viewPartial(string $viewPath, array $data = [])
    {
        return $this->view->viewPartial($viewPath, $data);
    }
}
