<?php

namespace Framework\Views\Psr;

/**
 * Description of IView
 *
 * @author root87x
 * 
*/

interface View
{
    public function setExtension(string $ext);
    
    public function setViewPath(string $path);
    
    public function setLayoutPath(string $path);
    
    public function addVar(string $name, $value);
    
    public function render(string $view, array $data = []);
    
    public function viewPartial(string $view, array $data = []);
}
