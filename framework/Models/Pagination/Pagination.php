<?php

namespace Framework\Models\Pagination;

use Framework\Models\Pagination\Drivers\Query;

class Pagination
{

    /**
     * 
     * @param type $entity
     * @return \Framework\Models\Pagination\Query
    */
    
    public function query($entity) : Query
    {
        return (new Query())->set($entity);
    }    
}
