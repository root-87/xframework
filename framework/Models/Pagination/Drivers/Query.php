<?php

namespace Framework\Models\Pagination\Drivers;

class Query
{
    /** @var \Illuminate\Database\Eloquent\Model */    
    private $queryObject;

    public $page = 1;
    
    protected $total = 0;
    protected $offset = 0;
    protected $limit = 12;
    
    public function __toString()
    {
        return self::class;
    }
        
    public function set($query) : self
    {
        $this->queryObject = $query;
        
        $this->total = $query->count();        
                
        return $this;
    }
    
    public function limit($count) : self
    {
        $this->limit = (int) $count;
        return $this;
    }

    public function page(int $page) : self
    {
        $this->page = $page;
        return $this;
    }

    public function countPages(): float
    {
        if ($this->limit > 0) {
            return ceil($this->total / $this->limit);
        };
        
        return 0;
    }
    
    public function totalItems() : int
    {
        return (int) $this->total;
    }
    
    public function offset(): int
    {
        return (int) $this->offset = ($this->page - 1) * $this->limit;
    }
    
    public function get()
    {
        return $this->queryObject->skip($this->offset)->take($this->limit)->get();
    }
}