<?php

namespace Framework\Models\Paginator;

use Framework\Models\Pagination\Pagination;

abstract 
class AbstractPaginator
{
    /** @var Pagination */ 
    protected $pagination;
    
    public function __construct($pagination)
    {
        $this->pagination = $pagination;
        
        $this->pagination->totalItems();
        $this->pagination->offset();
    }   
    
    /**
     * Всего элементов
     * 
     * @return type
     */
    
    public function totalItems(): int
    {
        return (int)$this->pagination->totalItems;
    }
    
    /**
     * 
     * @return int
    */
    public function totalPages(): int
    {
        return (int)$this->pagination->countPages();
    }
    
    /**
     * Текущая странца
     * 
     * @return type
    */
    
    public function currPage(): int
    {
        return (int) $this->pagination->page;
    }
    
    /**
     * Проверяем нужна ли кнопка назад
     * 
     * @return type
     */
    
    public function prevPage(): int
    {
        $prev = $this->pagination->page - 1;
        
        if (($this->pagination->page > 1) && ($this->pagination->page >= $prev))
        {
            return $prev;
        }
        
        return 0;
    }
    
    /**
     * Проверяем нужна ли кнопка вперёд
     * 
     * @return type
     */
    
    public function nextPage(): int
    {
        $next = $this->pagination->page + 1;
        
        if ($this->pagination->page < $this->totalPages())
        {
            return $next;
        }
        
        return 0;
    }
    
    /**
     * Общее кол-во страниц
     * 
     * @return array
    */
    public function pages(): array
    {
        $nums = [];
        
        for($i=1; $i < $this->totalPages() + 1; $i++) {
            $nums[] = $i;
        }
        
        return $nums;
    }   
}