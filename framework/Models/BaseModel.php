<?php

namespace Framework\Models;

use Framework\App;
use Valitron\Validator;

/**
 * Common of BaseModel
 *
 * @author root87x
 * @version 1.0.3
*/

class BaseModel extends \Illuminate\Database\Eloquent\Model
{
    /** @var array */
    protected $errors = [];
        
    /**
     * Правила для валидации
     * 
     * @return array
    */
    protected function rules()
    {
        return [];
    }
    
    /**
     * Подпись полей
     * 
     * @return array
    */
    protected function labels()
    {
        return [];
    }

    /**
     * установить атрибуты в storage
     * 
     * @param array $data
    */
    public function setAttributes(array $data): void
    {
        foreach ($data as $key => $value) {
            $this->attributes[$key] = $value;
        }
    }
    
    /**
     * Удаление аттрибута по ключу
     * 
     * @param string $key
    */
    public function removeAttribute(string $key): bool 
    {
        if (isset($this->attributes[$key])) {
            unset($this->attributes[$key]);
            return true;
        }
        return false;
    }
    
    /**
     * Вадижация модели
     * 
     * @return bool
    */
    public function validation(): bool
    {
        Validator::lang(App::$locale);
        
        $valid = new Validator($this->attributes);
        $valid->rules($this->rules());
        $valid->labels($this->labels());
            
        $result = $valid->validate();
        
        if (!$result) {
            $this->errors = $valid->errors();
        }

        return $result;
    }
    
    /**
     * Получить массив ошибок 
     * 
     * @return array
    */
    public function errors(): array
    {
        return $this->errors;
    }
}
