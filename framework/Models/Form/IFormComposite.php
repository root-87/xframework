<?php

namespace Framework\Models\Form;

/**
 * Паттерн композит для билда формы
 *
 * @author root87x
*/

interface IFormComposite
{
   public function build();
}
