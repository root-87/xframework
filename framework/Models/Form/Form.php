<?php

namespace Framework\Models\Form;

use Framework\Models\Form\IFormComposite;

/**
 * Генератор формы
 *
 * @author root87x <developer-home@yandex.ru>
 * 
 * @version 1.0.1
 * @package Models
 * @subpackage Form
*/

class Form implements IFormComposite
{
    /**
     * Данные формы
     * 
     * @access public
     * @var array 
    */
    public $data = [];
    
    private
       $id,     
       $action = '/',
       $method = 'GET',   
       $enctype = 'application/x-www-form-urlencoded';   
    
    /** 
     * Элементы формы
     * 
     * @access private
     * @var IFormComposite 
    */   
    private $elements;   
    
    /**
     * Создание экземпляра класса формы
     * 
     * @param string $id
     * @param array $data
    */
    
    public function __construct(string $id, array $data = [])
    {
        $this->id = $id;
        $this->data = $data;
    }
    
    /**
     * Установка адреса отправки формы 
     * 
     * @access public
     * @param string $url
    */
    public function setAction(string $url): void
    {
        $this->action = $url;
    }
    
    /**
     * Установка метода отправки данных
     * 
     * @access public
     * @param string $method
    */
    public function setMethod(string $method): void
    {
        $this->method = $method;
    }
    
    /**
     * Добавить элемент формы
     * 
     * @access public
     * @param IFormComposite $element
    */
    public function addElement(IFormComposite $element): void
    {
        $this->elements[] = $element;
    }

    /**
     * Генерация формы
     * 
     * @access public
     * @return string
    */
    public function build(): string
    {
        $form = '<form action="'.$this->action.'" method="'.$this->method.'" enctype="'.$this->enctype.'">';
        
        if (!empty($this->elements)) {

            foreach ($this->elements as $element)
            {            
                $element->value($this->data[$element->getName()] ?? null);
                $form .= $element->build();
            }
        }
           
        return $form;
    }
}
