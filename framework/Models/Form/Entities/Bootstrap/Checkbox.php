<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Framework\Models\Form\Entities\Bootstrap;

/**
 * Description of Checkbox
 *
 * @author root87x
 */
class Checkbox extends \Framework\Models\Form\Entities\Checkbox
{
    public function label(string $value)
    {
        $this->label = '<label for="id-'.$this->name.'" class="form-check-label">' . $value . '</label>';
        return $this;
    }
    
    public function build()
    {
        $this->attr(['class' => 'form-check-input']);
        return '<div class="form-check">' . parent::build() . '</div>';
    }
}
