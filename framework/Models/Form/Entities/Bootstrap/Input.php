<?php

namespace Framework\Models\Form\Entities\Bootstrap;

use Framework\Models\Form\Entities\Input as BaseInput;

/**
 * Description of Input
 *
 * @author root87x
*/

class Input extends BaseInput
{
    public function build()
    {
        $input = '<div class="form-group">';
            $input .= parent::build();
        $input .= '</div>';
        
        return $input;
    }
}