<?php

namespace Framework\Models\Form\Entities;

use Framework\Models\Form\IFormComposite;

/**
 * Description of Input
 *
 * @author root87x
*/

class Input implements IFormComposite
{
    protected 
        $name,
        $type,
        $value,
        $attributes;
    
    protected
        $label;    
          
    public function __construct($name, string $type = 'text', string $value = '')
    {
        $this->name = $name;
        $this->type = $type;
        $this->value = $value;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getValue()
    {
        return $this->value;
    }
    
    public function value($val)
    {
        $this->value = $val;
        return $this;
    }
    
    public function label(string $value)
    {
        $this->label = '<label for="id-'.$this->name.'">' . $value . '</label>';
        return $this;
    }
    
    public function attr(array $attributes)
    {
        foreach ($attributes as $key => $value) {
            $this->attributes .= $key . '="' . $value . '"';
        }
                
        return $this;
    }
    
    public function build()
    {
        $input = $this->label ?? '';
        $input .= '<input id="id-'.$this->name.'" type="'.$this->type.'" name="'.$this->name.'" value="'.$this->value.'" '.$this->attributes.'/>';
        
        return $input;
    }
}
