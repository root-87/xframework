<?php

namespace Framework\Models\Form\Entities;

use Src\App;
use Framework\Secure\Csrf;

/**
 * Description of Csrf
 *
 * @author root87x
 */
class CsrfInput extends Input 
{
    use Csrf;
    
    public function __construct() 
    {
        parent::__construct('csrf_token', 'hidden', $this->csrfoken());
    }

    public function value($val) 
    {
        return $this;
    }
}
