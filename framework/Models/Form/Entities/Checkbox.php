<?php

namespace Framework\Models\Form\Entities;

/**
 * Description of Checkbox
 *
 * @author root87x
 */
class Checkbox extends Input
{
    protected $checked;
    
    public function __construct($name = '')
    {
        parent::__construct($name, 'checkbox', '');
    }
    
    public function value($val)
    {
        if ($val) $this->checked = 'checked';
        return $this;
    }
        
    public function build()
    {
        $checkbox = '<input id="id-'.$this->name.'" name="'.$this->name.'" type="'.$this->type.'" ' . $this->attributes . ' '.$this->checked.' /> ';
        $checkbox .= $this->label ?? '';
        return $checkbox;
    }
}
