<?php

namespace Framework\Models\Form\Entities;

/**
 * Description of Submit
 *
 * @author root87x
*/
class Button extends Input
{
    public function __construct($name = null, string $type = 'button', string $value = 'Submit') 
    {
        parent::__construct($name, $type, $value);
    }
    
    public function value($val) 
    {
        return $this;
    }
    
    public function build()
    {
        return '<button type="'.$this->type.'" name="'.$this->name.'" ' . $this->attributes . '>'.$this->value.'</button>';
    }
}
