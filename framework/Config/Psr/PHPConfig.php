<?php

namespace Framework\Config\Psr;

/**
 *
 * @author root87x
 * @version 1.0.0
 */
interface PHPConfig
{
    public function setSource(array $source): void;
}
