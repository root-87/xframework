<?php

namespace Framework\Config\Psr;

/**
 * Description of Config
 *
 * @author root87x
*/

interface Config 
{
    public function get(string $key);
}
