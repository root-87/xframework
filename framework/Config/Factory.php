<?php

namespace Framework\Config;

use Framework\Config\Parts\PHPConfig;

/**
 * Description of FactoryAbstractConfig
 *
 * @author root87x
 * @version 1.0.1
 */

abstract 
class Factory
{    
    /**
     * PHP конфиг
     * @return PHPConfig
    */
    public static function phpConfig(): PHPConfig
    {
        return (new PHPConfig());
    }
}
