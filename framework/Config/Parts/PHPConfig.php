<?php

namespace Framework\Config\Parts;

/**
 * PHP Config
 *
 * @author root87x
 * @version 1.0.2
*/

class PHPConfig implements 
    \Framework\Config\Psr\Config, 
    \Framework\Config\Psr\PHPConfig
{
    /** @var array */
    private $source = [];
    
    /**
     * Файл конфига
     * 
     * @param array $source
     * @return void
    */
    public function setSource(array $source): void
    {
        $this->source = $source;
    }
    
    /**
     * 
     * @param string $key
     * @return mixed
    */
    public function get(string $key)
    {
        $storage = $this->source;
        $configKeys = explode('.', strtolower($key));
        
        foreach ($configKeys as $val) {
            if (isset($storage[$val])) {
                $storage = $storage[$val];
            } else {
                return null;
            }
        }
        
        return $storage;
    }
    
}
