<?php

$start = microtime(true); 

use Framework\App;
use Framework\Http\Request;

define('ROOT', dirname(__DIR__));

require_once ROOT . '/app/Bootstrap/Bootstrap.php';

$source = array_merge(
    require_once ROOT . '/app/Config/Main.php',
    require_once ROOT . '/app/Config/User.php'
);

$config = Framework\Config\Factory::phpConfig();
$config->setSource($source);

$request = new Request();
$request->setRouter(new \Bavix\Router\Router(
    $config->get('app.routes')
));

App::setLocale($request->attribute('lang'));

$application = new App($config);
$application->setRequest($request);

echo $application;

echo PHP_EOL . '<!-- Время выполнения скрипта: '. round(microtime(true) - $start, 3).' сек. -->'; 