<?php

use Framework\App;

define('ROOT', dirname(__DIR__));

require_once ROOT . '/app/Bootstrap/Bootstrap.php';

$source = array_merge(
    require ROOT . '/app/Config/Main.php',
    require ROOT . '/app/Config/User.php'
);

$config = Framework\Config\Factory::phpConfig();
$config->setSource($source);

new App($config);
