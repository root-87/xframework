    var url = "/" + lang + "/rbac/permissions";
    
    function refreshTable(data)
    {
        if (!data) return;

        let table = $('.permissions_list > tbody');
        
        $(table).html('');
        
        for(i=0; i < data.length; i++) {
            
            let row = '<tr id="'+data[i]['ID']+'">'+
                '<td>'+data[i]['ID']+'</td>'+
                '<td>'+data[i]['Title']+'</td>'+
                '<td>'+data[i]['Description']+'</td>'+
                '<td class="text-right">'+
                    '<a href="'+url+'/edit/'+data[i]['ID']+'" class="btn btn-success"><i class="fa fa-edit"></i></a>'+ ' '+
                    '<a href="javascript:void(0);" class="delete btn btn-danger" data-url="' + '/' + lang + '/rbac/permissions/delete/' + data[i]['ID']+'" data-question="Удалить ' + data[i]['Title'] + ' ?"><i class="fa fa-trash"></i></a>'+
                '</td>'+
            '</tr>';
            
            $(table).append(row);
        }
        
    }
 

$(function() {
   
    $('.form-permission').on('submit', function() {
        
        let form = new Form(this);
        
        new request().post(form.url, form.serializeArrayData).done(function(response) {
            
            let msg = response.msg;
            let error = new ErrorsHandler(response.errors);
                
            if (error.count > 0) {
                msg = error.first;
            }
            
            if (!response.result) {
                new notice().sweetAlert('Разрешения', msg, response.state);
            } else {
                new notice().sweetDialogInfo(msg, response.state).then((result) => {
                    if (response.flag === 'new') {
                        document.location.href = response.redirect;
                    }
                });
            }
            
        });
        
    });
    
    $('button.addpath').on('click', function() {
        
        let path = $('#perm_children').val();
        let id_permission = $('#id_permission').val();
        
        if (!path) return;

        let data = {
            'id_permission':id_permission, 
            'path':path
        };
        
        new request().post(url + '/addNode', data).done(function(response) {
            
            let msg = response.msg;
            let error = new ErrorsHandler(response.errors);

            if (error.count > 0) {
                msg = error.first;
            }
            
            if (response.result){
                
                let id_permission = $('#id_permission').val();
                
                new notice().sweetDialogInfo(msg, response.state).then((result) => {
                    
                    new request().get(url + "/nodes/" + id_permission).done(function(response) {
                        $('#perm_children').val('');
                        refreshTable(response);
                    });

                });
                
            } else {
                new notice().sweetAlert('Что то пошло не так', msg, response.state);
            }     
            
        });
    });
    
    $(document).on('click', '.permissions_list a.delete', function() {
        
        let url = $(this).data('url');
        let question = $(this).data('question');
        
        new notice().sweetConfirm(question, ' ' , 'info').then((result) => {
            
            if (result.value) {
                
                new request().delete(url, {}).done(function(response) {
                    
                    new notice().sweetDialogInfo(response.msg, response.state).then((result) => {
                        
                        if (response.result) {
                            
                            $('.permissions_list').find('tr#' + response.id).fadeOut(function() {
                                this.remove();  
                            });   
                        }
                        
                    });
                    
                });
            }
        });
    });
    
});