$(function() {
   
    $('.form-roles').on('submit', function() {
        
        let form = new Form(this);
        
        new request().post(form.url, form.serializeArrayData).done(function(response) {
            
            let msg = response.msg;
            let error = new ErrorsHandler(response.errors);
                
            if (error.count > 0) {
                msg = error.first;
            }
            
            if (!response.result) {
                new notice().sweetAlert('Роли', msg, response.state);
            } else {
                new notice().sweetDialogInfo(msg, response.state).then((result) => {
                    location.href = response.redirect_url;
                });
            }
            
        });
        
    });
    
    $('#role_list a.delete, .perm_children a.delete').on('click', function() {
        
        let url = $(this).data('url');
        let question = $(this).data('question');
        
        new notice().sweetConfirm(question, ' ' , 'info').then((result) => {
            
            if (result.value) {
                
                new request().delete(url, {}).done(function(response) {
                    
                    new notice().sweetDialogInfo(response.msg, response.state).then((result) => {
                        
                        if (response.result) {
                            location.reload();
                        }
                        
                    });
                    
                });
            }
        });
    });
    
});
