'use strict';

class Transfer {
    
    constructor(transFrom, transTo) {
        this.trans_from = transFrom;
        this.trans_to = transTo;
    }
    
    imp(url, from, to) {
        let trans_from = from;
        let trans_to = to;
        
        let list = $(trans_from).val();
        
        new request().post(url, {'data':list.toString()}).done(function(response) {
            if (response.result) {
                $(list).each(function(i,v) {
                    let to = null; 
                    let value = v.split('_');
                    if (value.length > 1) {
                        to = $(trans_to).find('optgroup[label="'+value[0]+'"]');
                    }
                    $(trans_from).find('option[value="'+v+'"]').appendTo(to);
                });
            }  
        });
    }
    
    move(url) {
        this.imp(url, this.trans_from, this.trans_to);
    };
    
    remove(url) {
        this.imp(url, this.trans_to, this.trans_from);
    };   
}