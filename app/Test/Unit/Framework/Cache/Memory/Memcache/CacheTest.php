<?php

namespace App\Test\Unit\Framework\Cache\Memory\Memcache;

use Framework\Cache\Memory\Memcache\Cache;

require_once 'web/app_test.php';

/**
 * Description of CacheTest
 *
 * @author root87x
 */
class CacheTest extends \PHPUnit\Framework\TestCase
{
    public function testTrueIsTrue()
    {
        $cache = new Cache();
        $cache->name('test');
        $cache->expiresAfter(100);
        $cache->set('test unit cache for memcache');
        
        $this->assertTrue($cache->isHit());
    }
}
