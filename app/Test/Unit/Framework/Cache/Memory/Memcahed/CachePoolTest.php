<?php

namespace App\Test\Unit\Framework\Cache\Memory\Memcached;

use Framework\Cache\Memory\Memcached\{
    Cache, 
    CachePool    
};

require_once 'web/app_test.php';

/**
 * Description of CachePool
 *
 * @author root87x
 */
class CachePoolTest extends \PHPUnit\Framework\TestCase
{
    public function testTrueIsTrue()
    {
        $cache = new Cache();
        $cache->name('test');
        $cache->expiresAfter(100);
        $cache->set('test unit cache for memcache');
        
        $cache = new CachePool();
        $result = $cache->hasItem('test');
        
        $this->assertTrue($result);
    }
}
