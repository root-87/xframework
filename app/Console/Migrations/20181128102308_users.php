<?php

use App\Console\Models\Migration;
use Illuminate\Database\Schema\Blueprint;

class Users extends Migration
{
    public function up()
    {
        $this->schema->table('users', function(Blueprint $table) {
            $table->increments('id');
            $table->string('login', 20)->unique();
            $table->string('email', 60)->unique();
            $table->string('password', 255);
            $table->smallInteger('active')->default(1);
            $table->datetime('created_at');
            $table->datetime('updated_at');
            $table->create();
        });
    }
    
    public function down()
    {
        $this->schema->table('users', function(Blueprint $table) {
           $table->dropIfExists(); 
        });
    }
}
