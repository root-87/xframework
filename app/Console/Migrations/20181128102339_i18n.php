<?php

use App\Console\Models\Migration;
use Illuminate\Database\Schema\Blueprint;

class I18n extends Migration
{
    public function up()
    {
        $this->schema->table('i18n', function(Blueprint $table) {
            $table->increments('id');
            $table->string('lang', 2);
            $table->string('key', 80);
            $table->text('value')->nullable();
            $table->string('path', 100);
            $table->create();
        });
        
        $this->schema->getConnection()->query('ALTER TABLE i18n ADD FULLTEXT key_fulltext_index (key)');
        $this->schema->getConnection()->query('ALTER TABLE i18n ADD FULLTEXT value_fulltext_index (value)');
    }
    
    public function down()
    {
        $this->schema->table('i18n', function(Blueprint $table) {
            $table->dropIfExists();
        });
    }
}
