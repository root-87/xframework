<?php

use App\Console\Models\Migration;
use Illuminate\Database\Schema\Blueprint;

class Rbac extends Migration
{
    public function up()
    {
        $path = dirname(__DIR__) . '/Data/Rbac';
        $files = array_diff(scandir($path), ['..','.']);
        
        foreach ($files as $file) {
            $this->execute(file_get_contents($path . '/' . $file));
        }
    }
    
    public function down()
    {
        $this->schema->table('rbac_permissions', function(Blueprint $table) {
            $table->dropIfExists();
        });
        
        $this->schema->table('rbac_rolepermissions', function(Blueprint $table) {
            $table->dropIfExists();
        });
        
        $this->schema->table('rbac_roles', function(Blueprint $table) {
            $table->dropIfExists();
        });
        
        $this->schema->table('rbac_userroles', function(Blueprint $table) {
            $table->dropIfExists();
        });
    }
}
