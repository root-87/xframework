<?php

use App\Console\Models\Migration;
use Illuminate\Database\Schema\Blueprint;

class Tokens extends Migration
{
    public function up()
    {
        $this->schema->table('tokens', function(Blueprint $table) {
            $table->increments('id');
            $table->string('token', 255);
            $table->integer('user_id');
            $table->datetime('created_at');
            $table->datetime('rip');
            $table->create();
        });
    }
    
    public function down()
    {
        $this->schema->table('tokens', function(Blueprint $table) {
            $table->dropIfExists();
        });
    }
}
