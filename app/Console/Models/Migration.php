<?php

namespace App\Console\Models;

use Illuminate\Database\Capsule\Manager as Capsule;
use Phinx\Migration\AbstractMigration;

/**
 * Class BaseMigration
 * @package Custodian\Console\Migrations
 */
class Migration extends AbstractMigration
{
    /**
     * @var \Illuminate\Database\Capsule\Manager $capsule
     */
    public $capsule;

    /**
     * @var \Illuminate\Database\Schema\Builder $capsule
     */
    public $schema;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $conf = require dirname(__DIR__, 2) . '/Config/Database.php';
        
        $this->capsule = new Capsule;
        $this->capsule->addConnection($conf);

        $this->capsule->bootEloquent();
        $this->capsule->setAsGlobal();
        $this->schema = $this->capsule->schema();
    }
}
