<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

$conf = require_once dirname(__DIR__, 2) . '/app/Config/Database.php';

if (empty($conf)) {
    return;
}

$logger = Logger('DB', ROOT . '/logs');

try {
    $capsule = new Capsule;
    $capsule->addConnection($conf);

    $capsule->setEventDispatcher(new Dispatcher(new Container));
    $capsule->setAsGlobal();
    $capsule->bootEloquent();
    $capsule->connection()->getPdO();
} catch (\PDOException $ex) {
    $logger->error($ex->getMessage());
} catch(\InvalidArgumentException $ex) {
    $logger->error($ex->getMessage());
} finally { 
    
}
