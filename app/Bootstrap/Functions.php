<?php

use Framework\Components\Exceptions\{
    NotFound,
    AccessDenied
};

/**
 * Логирование информации в файл
 * 
 * @param string $name
 * @param string $path
 * @return \Monolog\Logger
 */
function Logger(string $name, string $path)
{
    $logger = \Framework\Logs\LoggerFactory::FileLogger();
    $logger->setName($name);
    $logger->setPath($path);

    return $logger->getLogger();
}

/**
 * Исключение, страница не найдена
 * 
 * @param string $msg
 * @throws NotFound
 */
function ViewNotfound(string $msg)
{
    try {
        throw new NotFound($msg);
    } catch (NotFound $ex) {
        return $ex->view();
    }
}

/**
 * Исключение, нет доступа
 * 
 * @param string $msg
 * @throws PermissionDenied
 */
function ViewAccessDenied(string $msg)
{
    try {
        throw new AccessDenied($msg);
    } catch (AccessDenied $ex) {
        return $ex->view();
    }
}

/**
 * Криптография данных
 * 
 * @param type $data - Данные
 * @param type $key - ключ
 * @param type $method - метод шифрования
 * @return string|mixed
 */
function __openssl_encrypt(string $data, $key, string $method = 'AES-128-CBC')
{
    $encription_key = base64_decode($key);

    $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($method));

    $result = openssl_encrypt($data, $method, $encription_key, 0, $iv);

    return base64_encode($result . '::' . $iv);
}

/**
 * Раскодировать ключ
 * 
 * @param type $data - закриптованные данные
 * @param type $key - ключ
 * @param type $method - метод шифрования
 * @return string|mixed
 */
function __openssl_decrypt(string $data, $key, string $method = 'AES-128-CBC')
{
    $encryption_key = base64_decode($key);

    list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);

    return openssl_decrypt($encrypted_data, $method, $encryption_key, 0, $iv);
}

if (!function_exists('getallheaders')) {

    /**
     * Get all HTTP header key/values as an associative array for the current request.
     *
     * @return string[string] The HTTP header key/value pairs.
     */
    function getallheaders()
    {
        $headers = array();

        $copy_server = array(
            'CONTENT_TYPE' => 'Content-Type',
            'CONTENT_LENGTH' => 'Content-Length',
            'CONTENT_MD5' => 'Content-Md5',
        );

        foreach ($_SERVER as $key => $value) {
            if (substr($key, 0, 5) === 'HTTP_') {
                $key = substr($key, 5);
                if (!isset($copy_server[$key]) || !isset($_SERVER[$key])) {
                    $key = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', $key))));
                    $headers[$key] = $value;
                }
            } elseif (isset($copy_server[$key])) {
                $headers[$copy_server[$key]] = $value;
            }
        }

        if (!isset($headers['Authorization'])) {
            if (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])) {
                $headers['Authorization'] = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
            } elseif (isset($_SERVER['PHP_AUTH_USER'])) {
                $basic_pass = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : '';
                $headers['Authorization'] = 'Basic ' . base64_encode($_SERVER['PHP_AUTH_USER'] . ':' . $basic_pass);
            } elseif (isset($_SERVER['PHP_AUTH_DIGEST'])) {
                $headers['Authorization'] = $_SERVER['PHP_AUTH_DIGEST'];
            }
        }

        return $headers;
    }

}
