<?php

return [
    'app' => [
        'domain' => 'xframework',
        'name' => 'xFramework by root87x',
        'routes' => require_once dirname(__DIR__) . '/Routes/Routes.php',
        'secure' => [
            'openssl' => [
                'method' => 'sha256',
                'key' => 'xnCBLWXd5HhRGIgPxDYzOjG5eZscxwmv548MxPMM'
            ],
        ],
        'dicontainer' => [
            'rbac' => Src\Bundles\Rbac\Rbac::class,
            'example' => Src\Production\Production::class,
        ]
    ]
];