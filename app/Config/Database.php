<?php

use Dotenv\Dotenv;

$path = dirname(__DIR__);
$conf = [];

try {
    $dbconf = new Dotenv($path);
    $dbconf->load();

    array_walk($_ENV, function ($val, $key) use (&$conf) {
        $change_key = strtolower(str_replace('DB_', '', $key));
        $conf[$change_key] = $val;
    });

    return $conf;
} catch (\Dotenv\Exception\InvalidPathException $ex) {
    Logger('DB', ROOT . '/logs')->info($ex->getMessage());
} finally {
    
}
