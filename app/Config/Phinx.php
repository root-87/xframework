<?php

$conf = require __DIR__ . '/Database.php';

return [
    'migration_base_class' => 'app\Console\Models\Migration',
    'environments' => [
        'default_migration_table' => 'migrations',
        'default_database' => 'dev',
        'dev' => [
            'adapter' => $conf['driver'],
            'host' => $conf['host'],
            'name' => $conf['database'],
            'user' => $conf['username'],
            'pass' => $conf['password'],
            'port' => $conf['port'],
        ],
    ],
    'paths' => [
        'migrations' => 'app/Console/Migrations'
    ],
];