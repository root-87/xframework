<?php

return [
    'type' => 'prefix',
    'path' => '(/)',
    'resolver' => 
    [
        'Востановление пароля' => [
            'type' => 'prefix',
            'path' => '/forgotpassword',
            'resolver' => [
                'request' => [
                    'type' => 'pattern',
                    'path' => '/request',
                    'methods' => ['POST'],
                    'defaults' => [
                        'action' => 'request'
                    ]
                ],
                'view' => [
                    'type' => 'pattern',
                    'path' => '(/)',
                    'methods' => ['GET'],
                ]
            ],
            'defaults' => [
                'controller' => 'ForgotPassword',
                'action' => 'view',
            ]
        ],
        
        'Авторизация' => [
            'type' => 'prefix',
            'path' => '/auth',
            'resolver' => [
                'Авторизация' => [
                    'type' => 'pattern',
                    'path' => '/login',
                    'methods' => ['POST'],
                    'defaults' => [
                        'action' => 'login'
                    ]
                ],
                'Logout' => [
                    'type' => 'pattern',
                    'path' => '/logout',
                    'methods' => ['GET'],
                    'defaults' => [
                        'action' => 'logout'
                    ]
                ],
                'view' => [
                    'type' => 'pattern',
                    'path' => '(/)',
                    'methods' => ['GET'],
                ]
            ],
            'defaults' => [
                'controller' => 'Auth',
                'action' => 'view',
            ]
        ],
        
        'Регистрация' => [
            'type' => 'prefix',
            'path' => '/signup',
            'resolver' => [
                'Авторизация' => [
                    'type' => 'pattern',
                    'path' => '/reg',
                    'methods' => ['POST'],
                    'defaults' => [
                        'action' => 'reg'
                    ]
                ],
                'view' => [
                    'type' => 'pattern',
                    'path' => '(/)',
                    'methods' => ['GET'],
                ]
            ],
            'defaults' => [
                'controller' => 'Signup',
                'action' => 'view',
            ]
        ],
        
        'default' => [
            'type' => 'pattern',
            'path' => '(/<id:\d+>)',
            'defaults' => [
                'controller' => 'site',
                'action' => 'view',
            ]
        ]
    ],
    'defaults' => [
        'lang' => 'ru',
        'location' => 'Src\\Production\\Controllers'
    ]
];
