<?php

return [
    [
        'type' => 'prefix',
        'resolver' => [
            '/' => 
            [
                'type' => 'prefix',
                'path' => '(/<lang:[a-z]{2}>)(/)',
                'resolver' => [
                    'production' => include __DIR__ . '/Common/Production.php',
                    'rbac' => include __DIR__ . '/Bundles/Rbac.php'
                ]
            ]
        ]
    ]
];
