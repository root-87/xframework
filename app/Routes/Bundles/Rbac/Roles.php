<?php

return [
    
    'type' => 'prefix',
    'path' => '/roles',
    'resolver' =>
    [
        'save' => [
            'type' => 'pattern',
            'path' => '/save(/<id:\d+>)(/)',
            'methods' => ['POST'],
            'defaults' => [
                'action' => 'save'
            ]
        ],
        
        'assign' => [
            'type' => 'pattern',
            'path' => '/assign/<id:\d+>',
            'methods' => ['POST'],
            'defaults' => [
                'action' => 'assign'
            ]
        ],
        
        'unassign' => [
            'type' => 'pattern',
            'path' => '/unassign/<id:\d+>',
            'methods' => ['POST'],
            'defaults' => [
                'action' => 'unassign'
            ]
        ],
        
        'delete' => [
            'type' => 'pattern',
            'path' => '/delete/<id:\d+>',
            'methods' => ['DELETE'],
            'defaults' => [
                'action' => 'delete'
            ]
        ],
        
        'create' => [
            'type' => 'pattern',
            'path' => '/create',
            'methods' => ['GET'],
            'defaults' => [
                'action' => 'create'
            ]
        ],
        
        'edit' => [
            'type' => 'pattern',
            'path' => '/edit/<id:\d+>',
            'methods' => ['GET'],
            'defaults' => [
                'action' => 'edit'
            ]
        ],
        
        'view' => [
            'type' => 'pattern',
            'path' => '(/view)(/)',
            'methods' => ['GET'],
        ]
    ],
    'defaults' => [
        'controller' => 'roles',
    ]
    
];