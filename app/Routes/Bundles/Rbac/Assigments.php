<?php

return [
    'type' => 'prefix',
    'path' => '/assigments',
    'resolver' =>
    [
        'assign' => [
            'type' => 'pattern',
            'path' => '/assign/<id:\d+>',
            'methods' => ['POST'],
            'defaults' => [
                'action' => 'assign'
            ]
        ],
        'unassign' => [
            'type' => 'pattern',
            'path' => '/unassign/<id:\d+>',
            'methods' => ['POST'],
            'defaults' => [
                'action' => 'unassign'
            ]
        ],
        'edit' => [
            'type' => 'pattern',
            'path' => '/edit/<id:\d+>',
            'methods' => ['GET'],
            'defaults' => [
                'action' => 'edit'
            ]
        ],
        'view' => [
            'type' => 'pattern',
            'path' => '(/view)(/)',
            'methods' => ['GET'],
        ]
    ],
    'defaults' => [
        'controller' => 'assigments',
    ]
];
