<?php

return [
    'type' => 'prefix',
    'path' => '/permissions',
    'resolver' =>
    [
        'Сохранение дочерних разрешений' => [
            'type' => 'pattern',
            'path' => '/addNode(/)',
            'methods' => ['POST'],
            'defaults' => [
                'action' => 'addNode'
            ]
        ],
        
        'save' => [
            'type' => 'pattern',
            'path' => '/save(/<id:\d+>)(/)',
            'methods' => ['POST'],
            'defaults' => [
                'action' => 'save'
            ]
        ],
        
        'delete' => [
            'type' => 'pattern',
            'path' => '/delete/<id:\d+>',
            'methods' => ['DELETE'],
            'defaults' => [
                'action' => 'delete'
            ]
        ],
        
        'create' => [
            'type' => 'pattern',
            'path' => '/create',
            'methods' => ['GET'],
            'defaults' => [
                'action' => 'create'
            ]
        ],
        
        'edit' => [
            'type' => 'pattern',
            'path' => '/edit/<id:\d+>',
            'methods' => ['GET'],
            'defaults' => [
                'action' => 'edit'
            ]
        ],
        
        'Ноды' => [
            'type' => 'pattern',
            'path' => '/nodes/<id:\d+>',
            'methods' => ['GET'],
            'defaults' => [
                'action' => 'nodes'
            ]
        ],
        
        'view' => [
            'type' => 'pattern',
            'path' => '(/view)(/)',
            'methods' => ['GET'],
        ]
    ],
    'defaults' => [
        'controller' => 'permissions',
    ]
];