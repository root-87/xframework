<?php

return [
    'type' => 'prefix',
    'path' => '/rbac',
    'resolver' =>
    [
        'Назначения' => require_once __DIR__ . '/Rbac/Assigments.php',
        'Разрешения' => require_once __DIR__ . '/Rbac/Permissions.php',
        'Роли' => require_once __DIR__ . '/Rbac/Roles.php'
    ],
    'defaults' => [
        'action' => 'view',
        'lang' => 'ru',
        'bundle' => 'rbac',
        'location' => 'Src\\Bundles\\Rbac\\Controllers',
        'middleware' => [
            \Src\Bundles\Rbac\Middleware\Auth::class,
            \Src\Bundles\Rbac\Middleware\Access::class
        ]
    ]
];
