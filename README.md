#xFramework

##Карта проекта

- app/
	- Bootstrap/ ` Подключение файлов соединения, функций `
	- Config/ ` Файлы конфигурации `
	- Console/ ` Миграция `
	- Routes/ ` Маршруты `
	- example.env - Настройка соединения к базе данных
	
- docker/	
	- nginx/ ` Настройи web сервера nginx `
	- php-fpm/ ` Настройки php `
	
- framework/ ` Ядро `

- logs/ ` Логи `

- src/ ` Исходные файлы проекта `

- web/ 
	- assets/ ` Ассерты `
	- cache/ ` Файлы кэша `

##Установка с помощью Docker

1. В папке app/ переименовываем example.env в .env и изменяем данные по умолчанию на актуальные

2. docker-compose up --build -d

3. docker exec -it php composer install

4. docker exec -it php vendor/bin/phinx migrate -c app/Config/Phinx.php

