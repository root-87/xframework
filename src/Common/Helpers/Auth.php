<?php

namespace Src\Common\Helpers;

use Framework\Str;

use Src\Common\Models\{
    Tokens,
    Users
};

use Framework\Storage\Session\ {
    Session,
    SessionPool
};

use Framework\Storage\Cookie\{
    Cookie,
    CookiePool
};

/**
 * Аутентификация
 *  
 * @author root87x
*/

class Auth
{   
    /** @var string */
    private static $secureKey = null;
    
    /** @var int */
    private static $secure = 0;
    
    /**
     * Ключ для шифрования
     * @param string $key
    */
    public static function secure(string $key)
    {
        self::$secureKey = $key;
    }
    
    /**
     * Кастомная авторизация
     * 
     * @param array $params Условия
     * @return boolean
    */
    public static function attempt(...$params): bool
    {
        [$username, $password, $active, $remember] = $params;
        
        $auth = Users::where(['login' => $username, 'active' => $active])
                ->first(['id','login','password']);
        
        if (!$auth) {
            return false;
        }

        if (password_verify($password, $auth->password)) 
        {
            $token = static::saveAuthToken($auth->id);
            
            if (!$token) {
                return false;
            }
            
            unset($auth->password);
            
            if (self::$secureKey) {
                self::$secure = 1;
                $auth = __openssl_encrypt($auth, self::$secureKey);
            }

            if ($remember) {
                $profile = new Cookie('profile');
                $profile->httpOnly(true);
                $profile->set($auth);
            } else {
                $profile = new Session('profile');
                $profile->set($auth);
            }
   
            (new Session('access_token'))->set($token);
            (new Session('secure_account'))->set(self::$secure);
            
            return true;
        }

        return false;
    }
    
    /**
     * Проверка на авторизацию
     * @return bool
     */
    
    public static function check(): bool
    {
        $token = (new SessionPool(new Session('access_token')))->get();

        $user_id = (int) Users::storage('id');
        
        $auth = Tokens::where(['token' => $token, 'user_id' => $user_id])->rip()->count();        
        
        return (bool) $auth;
    }
    
    /**
     * Logout
     * @return bool
    */
    
    public static function logout(): bool
    {
        $cookie = new CookiePool(new Cookie('profile'));
        $session = new SessionPool(new Session('profile'));
        
        $sess_token = new SessionPool(new Session('access_token'));
        
        $token = Tokens::where('token', $sess_token->get())->first();
        
        $token->rip = date('Y-m-d H:i:s');
        
        if ((!$token) && (!$token->save())) {
            return false;
        }
        
        return (($cookie->delete() OR $session->delete()) && ($sess_token->delete()));
    }
    
    /**
     * Генерация авторизационного токена
     * 
     * @param int user id
     * @return string
    */
    
    private static function saveAuthToken(int $user_id): ?string
    {
        $model = new Tokens();
        $model->user_id = $user_id;
        $model->token = hash('sha256', Str::genString(14));  
        $model->created_at = (new \DateTime())->format('Y-m-d H:i:s');
        $model->rip = date('Y-m-d H:i:s', (time() + 3600 * 6));
        
        if(!$model->save()) {
            throw new \Exception('Неудалось получить токен');
        } 
        
        return $model->token;
    }
}
