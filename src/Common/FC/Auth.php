<?php

namespace Src\Common\FC;

use Framework\App;
use Src\Common\Helpers\Auth as HAuth;

/**
 * Модуль авторизации
 *
 * @author root87x
 * @version 1.0.1
*/

class Auth 
{    
    /**
     * Авторизация
     * 
     * @param string $username
     * @param string $password
     * @param bool $remember
     * @return bool
    */
    public function login(string $username, ?string $password, bool $remember = false): bool
    {
        $crypt_key = App::I()->conf('app.secure.openssl.key');
        
        HAuth::secure($crypt_key);

        return HAuth::attempt($username, $password, 1, $remember);
    }
    
    /**
     * Покануть сессию 
    */
    public function logout()
    {
        HAuth::logout();
    }
}
