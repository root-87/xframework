<?php

namespace Src\Common\Models;

use Src\Common\Models\Users;

/**
 * Description of LoginModel
 *
 * @author root87x
*/

class LoginModel extends Users
{
    use \Framework\Secure\Csrf;
    
    protected $table = 'users';

    protected function rules() 
    {
        return [
            'required' => [
                ['login'],['password'],['csrf_token']
            ],

            'contains' => [ ['csrf_token', $this->getCsrfToken()] ]
        ];
    }
    
    protected function labels() 
    {
        return [
            'login' => 'Логин',
            'password' => 'Пароль',
            'csrf_token' => 'Проверочный токен',
        ];
    }
}
