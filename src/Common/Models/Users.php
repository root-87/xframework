<?php

namespace Src\Common\Models;

use Framework\App;
use Framework\Secure\Access\Rbac\ControlAccess;
use Framework\Storage\Cookie\{
    Cookie, 
    CookiePool
};
use Framework\Storage\Session\{
    Session,
    SessionPool
};


/**
 * Description of Users
 *
 * @author root87x
 * @version 1.0.3
*/

class Users extends \Framework\Models\BaseModel
{    
    private static $storage = [];
    
    /**
     * Пользовательские данные текущей сессии
     * 
     * @param string $key
     * @return string|null
    */
    public static function storage(string $key): ?string
    {
        $secure = (bool)(new SessionPool(new Session('secure_account')))->get();
        
        $data = new CookiePool(new Cookie('profile'));
        
        if (!$data->has()) {
            $data = new SessionPool(new Session('profile'));
        }
        
        $user_data = $data->get();
        
        if (!$user_data) {
            return null;
        }

        if ($secure) {
            $openssl_key = App::I()->conf('app.secure.openssl.key');
            self::$storage = json_decode(__openssl_decrypt($user_data, $openssl_key), true);
        } else {
            self::$storage = json_decode($user_data, true);
        }

        return self::$storage[$key] ?? null;
    }
    
    /**
     * Установка пароля
     * 
     * @param string $password
    */
    public function setPassword(string $password)
    {
        $this->password = password_hash($password, PASSWORD_ARGON2I, ['cost' => 12]);
    }
    
    /**
     * Проверить права доступа
     * 
     * @param string $permission
     * @return type
     * @throws \Exception
    */
    public function checkAccess(string $permission)
    {
        $user_id = $this->id ?? self::storage('id');
        return (new ControlAccess())->check($permission, $user_id);
    }
}
