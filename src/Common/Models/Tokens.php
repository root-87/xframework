<?php

namespace Src\Common\Models;

use Framework\Models\BaseModel;

/**
 * Description of Tokens
 *
 * @author root87x
 */
class Tokens extends BaseModel
{
    public $timestamps = false;
    
    public function scopeRip($query)
    {
        return $query->where('rip','>', date('y-m-d H:i:s'));
    }
    
}
