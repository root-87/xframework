<?php

namespace Src\Common\Models;

use Src\Common\FC\Translater\Illuminate\i18n;
use Src\Common\Models\Users;

/**
 * Description of SignupModel
 *
 * @author root87x
 */
class SignupModel extends Users
{
    protected $table = 'users';
    
    protected function rules() 
    {
        return [
            'required' => [
                ['login'],['password'],['email'],['password_repeat']
            ],
            'equals' => [
                ['password','password_repeat']
            ],
            'email' => [
                ['email']
            ]
        ];
    }

    protected function labels() 
    {
        return [
            'login' => 'Имя пользователя',
            'email' => 'Почтовый ящик',
            'password' => 'Пароль',
            'password_repeat' => 'Повторите'
        ];
    }
        
    /**
     * Инициализация события
     * @return void
    */
    protected static function boot(): void
    {
        parent::boot();
        
        static::creating(function(SignupModel $model) {
            $model->removeAttribute('password_repeat');
        });
    }
    
    /**
     * Сохранение пользователя
     * 
     * @param array $options
     * @return boolean
    */
    public function save(array $options = array())
    {
        try {
            return parent::save($options);
        } catch (\Illuminate\Database\QueryException $ex) {
            Logger('account', ROOT . '/logs')->info($ex->getMessage());
        } finally {
            $this->errors[] = 'Такой пользователь уже зарегистрирован';
            
        }
        
        return false;
    }
}
