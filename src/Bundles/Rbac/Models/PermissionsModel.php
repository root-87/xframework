<?php

namespace Src\Bundles\Rbac\Models;

use Illuminate\Database\Capsule\Manager as DB;

/**
 * Модель of PermissionsModel
 *
 * @author root87x
*/

class PermissionsModel extends \Framework\Models\BaseModel
{
    protected $table = 'rbac_permissions';
    
    protected function rules()
    {
        return [
            'required' => [
                ['Title']
            ]
        ];
    }

    protected function labels()
    {
        return [
            'Title' => 'Название'
        ];
    }
}
