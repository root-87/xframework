<?php

namespace Src\Bundles\Rbac\Models;

use Valitron\Validator;
use PhpRbac\Rbac;

/**
 * Description of Role
 *
 * @author root87x
*/

class RoleModel extends \Framework\Models\BaseModel
{
    protected $table = 'rbac_roles';
    
    protected function rules()
    {
        return [
            'required' => [
                ['Title']
            ]
        ];
    }
    
    protected function labels()
    {
        return [
            'Title' => 'Название'
        ];
    }
}
