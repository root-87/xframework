<?php

namespace Src\Bundles\Rbac\Models\Rules;

use Valitron\Validator;
use Framework\Models\Rules;

/**
 * Description of Users
 *
 * @author root87x
*/

class RuleRoles extends Rules
{
    protected function def()
    {
        $validator = new Validator($this->model->getAttributes());
        $validator->rule('required', ['Title']);
        return $validator;
    }
    
}
