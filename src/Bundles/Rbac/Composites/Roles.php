<?php

namespace Src\Bundles\Rbac\Composites;

/**
 * Description of Permissions
 *
 * @author root87x
*/

class Roles extends RbacComposites
{
    public function assign()
    {
        foreach($this->data['permissions'] as $index => $id_perm) {
            $this->result = $this->rbac->Roles->assign($this->id, $id_perm);
        }
        
        return $this->result;
    }
    
    public function unassign() 
    {
        foreach($this->data['permissions'] as $index => $id_perm) {
            $this->result = $this->rbac->Roles->unassign($this->id, $id_perm);
        }
        
        return $this->result;
    }
}
