<?php

namespace Src\Bundles\Rbac\Composites;

/**
 * Description of Assigments
 *
 * @author root87x
 */
class Assigments extends RbacComposites
{
    public function assign()
    {
        foreach($this->data['roles'] as $index => $id_role) {
            $this->result = $this->rbac->Users->assign($id_role, $this->id);
        }
        
        return $this->result;
    }
    
    public function unassign() 
    {
        foreach($this->data['roles'] as $index => $id_role) {
            $this->result = $this->rbac->Users->unassign($id_role, $this->id);
        }
        
        return $this->result;
    }
}
