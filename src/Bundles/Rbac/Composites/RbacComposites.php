<?php

namespace Src\Bundles\Rbac\Composites;

use PhpRbac\Rbac;

/**
 * Description of AssignFactory
 *
 * @author root87x
*/

abstract 
class RbacComposites
{
    protected $id;
    protected $data = [];
    protected $rbac;
    protected $result = false;


    public function __construct(int $id, string $data) 
    {
        $this->id = $id;
        $this->data = $this->parseAssignData($data);
        $this->rbac = new Rbac();
    }
    
    protected function parseAssignData(string $string_data)
    {
        $entities = [];
        $to_array = explode(',', $string_data);
       
        foreach ($to_array as $index => $item) {
            list($group, $path) = explode('_', $item);
            $entities[$group][] = $path;
        }

        return $entities;
    }

    abstract public function assign();
    abstract public function unassign();
}
