<?php

namespace Src\Bundles\Rbac\Middleware;

use Src\Common\Helpers\Auth AS HAuth;


/**
 * Description of Auth
 *
 * @author root87x
 * @version 1.0
*/
class Auth
{
    public function __invoke(\Framework\Http\Request $request)
    {
        if (!HAuth::check()) {
            return $request->redirect()->go301('/auth');
        }
    }
}
