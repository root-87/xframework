<?php

namespace Src\Bundles\Rbac\Middleware;

use Framework\Http\Url;
use Framework\Secure\Access\Rbac\ControlAccess;
use Src\Common\Models\Users;

/**
 * Description of Access
 *
 * @author root87x
 */
class Access
{
    private $check = true;
    
    public function __invoke(\Framework\Http\Request $request)
    {
        $controller = $request->attribute('controller');
        $view = $request->attribute('action');
        
        if (!$this->check OR strtolower($controller) === 'auth') {
            return;
        }

        $user = new Users();
        $access = new ControlAccess();
        
        $user_id = (int)$user->storage('id');
        
        $check = $access->check("/rbac/{$controller}/{$view}", $user_id);

        if (!$check) {
            $request->redirect()->go301(Url::to('/auth'));
        } 
    }
}
