<?php

namespace Src\Bundles\Rbac;

use Framework\Views\ViewAdapter;
use Framework\Views\Templates\Twig;

/**
 * Rbac Bundle
 *
 * @author root87x
*/

class Rbac
{
    const VIEW = __DIR__ . '/Views';
    const LAYOUTS = './../src/Production/Views/Layouts';
    
    public function getView()
    {
        $view = new ViewAdapter(new Twig());
        $view->setViewPath(self::VIEW);
        $view->setLayoutPath(self::LAYOUTS);
        
        $view->registerObject('url', \Framework\Http\Url::class);
        $view->registerObject('auth', \Src\Common\Helpers\Auth::class);
        
        return $view;
    }
}
