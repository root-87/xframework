<?php

namespace Src\Common\FC\Html;

/**
 * Description of Html
 *
 * @author root87x
 */
class Html extends HtmlSupport
{
    public static function link(string $text, string $href, array $attrs)
    {
        return static::openTag('a href="'.$href.'" ', $attrs) . $text . static::closeTag('a');
    }
    
    public static function button(string $text, array $attrs)
    {
        return static::openTag('button', $attrs) . $text . static::closeTag('button');
    }
}
