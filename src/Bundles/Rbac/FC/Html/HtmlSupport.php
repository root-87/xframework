<?php

namespace Src\Common\FC\Html;
/**
 * Description of HtmlFactory
 *
 * @author root87x
 */

abstract 
class HtmlSupport
{
    /**
     * Открытие тега
     * 
     * @param type $tag - имя тега
     * @param type $params - аттрибуты
     * @return string
    */
    
    public static function openTag($tag, array $attrs) : string
    {
        $iterator = new \RecursiveArrayIterator($attrs);
        return '<' . $tag . static::attributesFormatter($iterator) . '>';
    }
    
    public static function closeTag($tag) : string
    {
        return '</' . $tag . '>';
    }
    
    private static function attributesFormatter(\RecursiveArrayIterator $iterator) : string
    {
        $to_string = '';
        
        while ($iterator->valid()) { 
            if (!$iterator->hasChildren()) {
                $to_string .= ' ' . $iterator->key() . '="' . $iterator->current() . '"' . PHP_EOL;          
            } else {
                self::attributesFormatter($iterator->current());
            }
            $iterator->next();
         }

        return $to_string;
    }
}
