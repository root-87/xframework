<?php

namespace Src\Bundles\Rbac\FC\Buttons;

use Src\Bundles\Rbac\FC\Html\Html;

/**
 * Description of ButtonsEntities
 *
 * @author root87x
 */

abstract 
class ButtonFormatter extends Formatter
{
    protected 
        $name,
        $text;
    
    public function __construct($name, $text)
    {
        $this->name = $name;
        $this->text = $text;
    }
    
    public function get()
    {
        return Html::button($this->text, $this->attributes);
    }
}
