<?php

namespace Src\Bundles\Rbac\FC\Buttons;

/**
 * Description of Formatter
 *
 * @author root87x
*/

abstract 
class Formatter
{
    protected $attributes = [];
    
    
    /**
     * Добавить аттрибут
     * 
     * @param string $name Название
     * @param string $value Значение
     * @return $this
    */
    public function addAttr(string $name, string $value)
    {
        $this->attributes[$name] = $value;
        return $this;
    }
}
