<?php

namespace Src\Bundles\Rbac\FC\Buttons\Bootstrap;

use Src\Bundles\Rbac\FC\Buttons\LinkFormatter;
use Src\Bundles\Rbac\FC\Buttons\Interfaces\PoolStorages;

/**
 * Description of LinkSuccess
 *
 * @author root87x
 */
class Link extends LinkFormatter implements PoolStorages
{
    /**
     * Соберает bootstrap зелённую кнопку из ссылки
     * 
     * @param type $href - href ссылка на другую страницу
     * @param type $text - Содержание
     */
    
    public function __construct(string $href, string $text, array $attributes = [])
    {
        parent::__construct($href, $text, $attributes);
    }
}
