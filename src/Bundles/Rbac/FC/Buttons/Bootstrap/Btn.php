<?php

namespace Src\Bundles\Rbac\FC\Buttons\Bootstrap;

use Src\Bundles\Rbac\FC\Buttons\ButtonFormatter;
use Src\Bundles\Rbac\FC\Buttons\Interfaces\IButtons;

/**
 * Description of BtnPrimary
 *
 * @author root87x
*/

class Btn extends ButtonFormatter implements IButtons
{
    /**
     * Соберает bootstrap синию кнопку
     * 
     * @param string $name - Аттрибут name
     * @param string $text - Содержание
    */
    
    public function __construct(string $name, string $text, array $params)
    {
        parent::__construct($name, $text, $params);
    }
}
