<?php

namespace Src\Bundles\Rbac\FC\Buttons;

use Src\Bundles\Rbac\FC\Html\Html;

/**
 * Description of LinkFormatter
 *
 * @author root87x
 */
abstract 
class LinkFormatter extends Formatter
{
    private $href;
    private $text;
    
    public function __construct($href, $text, $attributes)
    {
        $this->href = $href;
        $this->text = $text;
        
        if (!empty($attributes)) {
            foreach ($attributes as $key => $value) {
                $this->addAttr($key, $value);
            }
        }
    }
    
    public function get()
    {
        return Html::link($this->text, $this->href, $this->attributes);
    }
}
