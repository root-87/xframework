<?php

namespace Src\Bundles\Rbac\FC\Buttons;

/**
 * Button factory
 *
 * @author root87x
 * @version 1.0.1
 */
abstract 
class ButtonFactory
{
    protected $storage;
    private $format;

    public function add($button) {
        $this->storage[] = $button;
    }
    
    /**
     * Получить кнопки
     * @param bool $link Если true вернёт link button иначе button
    */
    
    public function get()
    {
        if (empty($this->storage)) return;
        
        foreach ($this->storage as $key => $button)
        {
            if ($button instanceof Interfaces\PoolStorages)
            {
                $this->format[] = $button->get();
            }
        }
        
        return $this->format;
    }
    
    public function removeByIndex(int $index)
    {
        unset($this->storage[$index]);
    }
    
    public function clear() 
    {
        unset($this->storage);
    }
}
