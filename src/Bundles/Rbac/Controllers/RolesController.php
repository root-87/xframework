<?php

namespace Src\Bundles\Rbac\Controllers;

use Src\Common\FC\Buttons\Button;
use Src\Common\FC\Buttons\Bootstrap\Link;
use Src\Common\FC\Translater\Illuminate\i18n;

use Src\Bundles\Rbac\Composites\Roles;
use Src\Bundles\Rbac\Models\RoleModel;
use Src\Bundles\Rbac\Models\PermissionsModel;

use Framework\Http\{Url, Request, Response};
use Framework\Exceptions\NotFound;

/**
 * Description of RolesController
 *
 * @author root87x
 */
class RolesController extends Controller
{       
    /** @var RoleModel */
    private $model;

    public function beforeAction() 
    {
        $this->model = new RoleModel();
    }
    
    public function actions(Request $request): array
    {
        return [
            'create' => function() use ($request) {
                $title = 'Новая роль';
                $this->view->addVars(['title' => $title, 'header' => $title]);
            },
            
            'edit' => function() use ($request) {
                $this->model = RoleModel::findOrFail((int)$request->attribute('id'));
                $title = 'Редактировать ' . $this->model->Title;
                $this->view->addVars(['title' => $title, 'header' => $title]);
            },
                    
            'view' => function() use ($request) {
                $title = 'Rbac роли';
                $this->view->addVars(['title' => $title, 'header' => $title]); 
            }
        ];
    }


    /**
     * Создание / обновление
     *  
     * @param Request $request
     * @param Response $response
     * 
     * @return type
     */
    
    public function save(Request $request, Response $response)
    {
        $result = false;

        $this->model->setAttributes($request->post());
        
        $id = (int)$request->attribute('id');    
        
        if ($this->model->validation()) { 
            if ($id) {
                $result = $this->rbac->Roles->edit($id, $this->model->Title, $this->model->Description);
            } else {
                $result = $this->rbac->Roles->add($this->model->Title, $this->model->Description);
            }
        }
        
        if ($result) {
            $msg = $this->i18n::t('rbac.roles.save.success','Роль :item сохранена', [':item' => $this->model->Title]);
        } else {
            $msg = $this->i18n::t('rbac.roles.save.fail','Неудалось сохранить :item', [':item' => $this->model->Title]);
        }
        
        $role_id = $id ? $id : $result;
        
        return $response->json([
            'result' => (bool)$result,
            'msg' => $msg,
            'redirect_url' => Url::to('/rbac/roles/edit/' . $role_id),
            'errors' => $this->model->errors(),
            'state' => (bool)$result ? 'success' : 'error'
        ]);
    }
    
    /**
     * Удаление роли
     * 
     * @param \Framework\Http\Request $request
     * @return type
     */
    
    public function delete(Request $request, Response $response)
    {
        $id_role = (int) $request->attribute('id');
        
        if (!$id_role) {
            throw new \Exception('No id');
        }
        
        $name = $this->rbac->Roles->getTitle($id_role);
        $result = $this->rbac->Roles->remove($id_role);
        
        if ($result) {
            $msg = $this->i18n::t('rbac.role.delete.success', ':item успешно', [':item' => $name]);
        } else {
            $msg = $this->i18n::t('rbac.role.delete.success', 'Что то пошло не так');
        }
        
        return $response->json([
            'result' => $result,
            'msg' => $msg,
            'state' => $result ? 'success' : 'error'
        ]);
    }
    
    /**
     * Создание
     * @param Request $request
     * @param Response $response
     * 
     * @return type
     */
    
    public function create(Request $request, Response $response)
    {
        return $this->render('edit', [
            'model' => $this->model,
            'action' => Url::to('/rbac/roles/save')
        ]);
    }
    
    /**
     * Изменение
     * 
     * @param Request $request
     * @param Response $response
     * 
     * @return type
     * @throws NotFound
     */
    
    public function edit(\Framework\Http\Request $request)
    {        
        $role_id = (int)$request->attribute('id');
        
        $new_rezerv = [];
        $permission_used = [];
        
        $perm_used = $this->rbac->Roles->permissions($role_id, false);
        $not_in = !empty($perm_used) ? array_column($perm_used, 'ID') : []; 
        $rezerv = PermissionsModel::whereNotIn('ID', $not_in)->get();
        
        if ($rezerv) {
            foreach($rezerv->toArray() as $rezerv) {
                $new_rezerv[] = $this->rbac->Permissions->getPath($rezerv['ID']);
            } 
            sort($new_rezerv);
        }
        
        if (!empty($perm_used)) {
            foreach($perm_used as $perm_use) {
                $permission_used[] = $this->rbac->Permissions->getPath($perm_use['ID']);
            }
            sort($permission_used);
        }
        
        return $this->render('/Roles/Edit', [
            'model' => $this->model, 
            'action' => Url::to('/rbac/roles/save/' . $role_id),
            'permissions' => [
                'reservs' => $new_rezerv,
                'used' => $permission_used
            ]
        ]);
    }
    
    /**
     * Добавление разрешений к роли
     * 
     * @param Request $request
     * @param Response $response
     * 
     * @return string
     */
    
    public function assign(Request $request, Response $response): string
    {
        $id_role = $request->attribute('id');  
        $roles = new Roles($id_role, $request->post('data', [], true));
        
        return $response->json([
            'result' => $roles->assign()
        ]);
    }
    
    /**
     * Удаление разрешений из роли
     * 
     * @param Request $request
     * @param Response $response
     * 
     * @return string
     */
    
    public function unassign(Request $request, Response $response): string
    {
        $id_role = $request->attribute('id');
        $roles = new Roles($id_role, $request->post('data'));
        
        return $response->json([
            'result' => $roles->unassign()
        ]);
    }
    
    /**
     * Список ролей
     * 
     * @param Request $request
     * @param Response $response
     * @return type
     */
    
    public function view(Request $request, Response $response) 
    {
        $roles = RoleModel::all(['ID','Title','Description']);
        
        return $this->render('/Roles/Index', [
            'roles' => $roles,
            'access_edit' => $this->user->checkAccess('/rbac/roles/edit'),
            'access_delete' => $this->user->checkAccess('/rbac/roles/delete')
        ]);
    }
}