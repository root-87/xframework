<?php

namespace Src\Bundles\Rbac\Controllers;

use Framework\Http\{Url, Request, Response};
use Framework\Components\Exceptions\NotFound;

use Src\Bundles\Rbac\Models\PermissionsModel;
use Src\Common\FC\Buttons\Button;
use Src\Common\FC\Buttons\Bootstrap\Link;

/**
 * Description of PermissionsController
 *
 * @author root87x
*/

class PermissionsController extends Controller
{   
    /** @var PermissionsModel */
    private $model;
    
    public function actions(Request $request): array
    {
        return [
            'edit' => function() use ($request) {
                $this->model = PermissionsModel::findOrFail((int)$request->attribute('id'));
                $title = 'Редактировать ' . $this->model->Title;
                $this->view->addVars(['title' => $title, 'header' => $title]);
            },     
            'view' => function() use ($request) {
                $title = 'Разрешения';
                $this->view->addVars(['title' => $title, 'header' => $title]);
            }
        ];
    }
    
    public function save(Request $request, Response $response)
    {
        $result = false;
                
        $this->model->setAttributes($request->post());
        
        if ($this->model->validation()) {
            if ($this->model->ID) {
                $result = $this->rbac->Permissions->edit($this->model->ID, $this->model->Title, $this->model->Description);
            } else {
                $result = $this->rbac->Permissions->add($this->model->Title, $this->model->Description);
            }
        }
        
        if ($result) {
            $msg = 'Сохранено ' . $this->model->Title;
        } else {
            $msg = 'Неудалось ' . $this->model->Title;
        }
        
        $id = $this->model->ID ?? $result;
        
        return $response->json([
            'result' => $result,
            'id' => $id,
            'flag' => $result === $id ? 'new' : 'edit',
            'redirect' => Url::to('/rbac/permissions/edit/' . $id),
            'errors' => $this->model->errors(),
            'msg' => $msg,
            'state' => (bool)$result ? 'success' : 'error'
        ]);
    }
    
    /**
     * Возвращает список дочерних правил 
     * 
     * @param \Framework\Http\Request $request
     * @return type
     * @throws \Exception
     */
    
    public function nodes(Request $request, Response $response)
    {
        $id_permission = (int) $request->attribute('id');
        
        $children = $this->rbac->Permissions->children($id_permission);
        
        return $response->json($children);
    }
    
    /**
     * Добавляет дочение правила к правилу
     * 
     * @param \Framework\Http\Request $request
     * @return type
     * @throws \Exception
    */
    
    public function addNode(Request $request, Response $response)
    {
        $result = false;
 
        $id_permission = (int)$request->post('id_permission');

        $data_validate = new \Valitron\Validator($request->post());
        $data_validate->rule('required', ['path'])->label('Дочерние разрешения не можеть быть пустым');
        
        if ($data_validate->validate()) {
            foreach (explode('/', $request->post('path')) as $path) {
                $path = lcfirst($path);
                $result = (bool) $this->rbac->Permissions->add($path, '', $id_permission);
            }
        } 
        
        if ($result) {
            $msg = $this->i18n::t('rbac.permissions.addnode.success',':item успешно добавлено', [':item' => $path]);
        } else {
            $msg = $this->i18n::t('rbac.permissions.addnode.fail',':item не добавлено', [':item' => $path]);
        }
        
        return $response->json([
            'result' => $result, 
            'errors' => $data_validate->errors(),
            'state' => $result ? 'success' : 'error',
            'msg' => $msg
        ]);
        
    }
    
    /**
     * Удаляет правило, если правило содержит дочерние правило оно так же удаляет их
     * 
     * @param \Framework\Http\Request $request
     * @return type
    */
    
    public function delete(Request $request, Response $response)
    {
        $id_permission = (int) $request->attribute('id');
        $result = $this->rbac->Permissions->remove((int)$id_permission, true);
        
        if ($result) {
            $msg = $this->i18n::t('permissions.delete.success','Разрешение удалено');
        } else {
            $msg = $this->i18n::t('permissions.delete.fail','Неудалось удалить разрешение');
        }
        
        return $response->json([
            'result' => $result, 
            'id' => $id_permission,
            'msg' => $msg
        ]);
    }
    
    /**
     * Редактирование правила
     * 
     * @param \Framework\Http\Request $request
     * @return type
     * @throws NotFound
     */
    
    public function edit(Request $request, Response $response)
    {           
        return $this->render('/Permissions/Edit', [
            'model' => $this->model,
            'used_roles' => $this->rbac->Permissions->roles($this->model->ID, false), 
            'children' => $this->rbac->Permissions->children($this->model->ID),
            'access_edit' => $this->user->checkAccess('/rbac/permissions/edit'),
            'access_delete' => $this->user->checkAccess('/rbac/permissions/delete')
        ]);
    }
    
    /**
     * Создаёт новое правило
     * 
     * @param \Framework\Http\Request $request
     * @return type
     */
    
    public function create(Request $request, Response $response)
    {  
        return $this->render('/Permissions/Edit', [
            'model' => new PermissionsModel()
        ]);
    }
    
    /**
     * Список правил
     * 
     * @param \Framework\Http\Request $request
     * @return type
     */
    
    public function view(Request $request, Response $response)
    {
        return $this->render('/Permissions/Index', [
            'permissions' => $this->rbac->Permissions->children(1),
            'access_edit' => $this->user->checkAccess('/rbac/permissions/edit'),
            'access_delete' => $this->user->checkAccess('/rbac/permissions/delete')
        ]);
    }
}
