<?php

namespace Src\Bundles\Rbac\Controllers;

use Framework\Views\ViewAdapter;

use Src\Common\Helpers\Auth;
use Src\Common\Models\Users;

use PhpRbac\Rbac as PHPRbac;

/**
 * Description of Controller
 *
 * @author root87x
 * @version 1.0.4
 */
abstract 
class Controller implements \Framework\Psr\Controller
{
    /**
     * Адаптер view
     * @var ViewAdapter 
    */
    protected $view;
    
    /** @var Users */
    protected $user;
    
    /** @var PHPRbac */
    protected $rbac;

    public function __construct(\Framework\Storage\DIContainer $container)
    {
        $this->view = $container->get('rbac')->getView();  
        
        $this->user = new Users();
        $this->user->isAuth = Auth::check();
        
        $this->rbac = new PHPRbac();
    }
    
    /**
     * Отобразить шаблон
     * 
     * @param string $view
     * @param array $data
     * @return type
    */
    protected function render(string $view, array $data = [])
    {
        return $this->view->render($view, $data);
    }
    
    /**
     * Буфферезировать шаблон
     * 
     * @param string $view
     * @param array $data
     * @return type
    */
    protected function buffer(string $view, array $data = [])
    {
        ob_start();
            $content = $this->render($view, $data);
        ob_get_contents();
        
        return $content;
    }
}
