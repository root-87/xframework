<?php

namespace Src\Bundles\Rbac\Controllers;

use Framework\Http\{
    Url, 
    Request, 
    Response    
};

use Framework\Components\Exceptions\NotFound;

use Src\Common\Models\Users;

use Src\Bundles\Rbac\{
    Composites\Assigments,
    Models\RoleModel
};

/**
 * Description of UsersController
 *
 * @author root87x
 */
class AssigmentsController extends Controller
{

    public function actions(Request $request): array 
    {
        return [
            'edit' => function () use ($request) {
                $this->userModel = Users::findOrFail((int)$request->attribute('id'));
                $title = 'Редактировать назначение ' . $this->userModel->login;
                $this->view->addVars(['title' => $title, 'header' => $title]);
            },
            'view' => function () use ($request) {
                $title = 'Назначения';
                $this->view->addVars(['title' => $title, 'header' => $title]);
            }
        ];
    }
    
    /**
     * Редактирование назначения
     * 
     * @param \Framework\Http\Request $request
     * @return type
     * @throws NotFound
    */
    
    public function edit(Request $request, Response $response)
    {
        $roles = $this->rbac->Users->allRoles((int)$request->attribute('id'));
        $whereNot = $roles ? array_column($roles, 'ID') : [];

        return $this->render('/Assigments/Edit', [
            'model' => $this->userModel, 
            'roles' => [
                'reserv' => RoleModel::whereNotIn('ID', $whereNot)->get()->toArray(),
                'used' => $roles ?? [],
            ]
        ]);
    }
    
    /**
     * Добавление ролей к назначению
     * 
     * @param \Framework\Http\Request $request
     * @return mixed
    */
    
    public function assign(Request $request, Response $response)
    {
        $id_user = (int) $request->attribute('id');       
        $result = new Assigments($id_user, $request->post('data'));
        
        return $response->json([
            'result' => $result->assign()
        ]);
    }
    
    /**
     * Удаление ролей из назначения
     * 
     * @param \Framework\Http\Request $request
     * @return mixed
    */
    
    public function unassign(Request $request, Response $response)
    {
        $id_user = (int) $request->attribute('id');
        $result = new Assigments($id_user, $request->post('data'));
        
        return $response->json([
            'result' => $result->unassign()
        ]);
    }
    
    /**
     * Список назначений 
     * 
     * @param \Framework\Http\Request $request
     * @return type
     */
    
    public function view(Request $request, Response $response)
    {
        return $this->render('/Assigments/Index', [
            'users' => Users::all(),
            'editAccess' => $this->user->checkAccess('/rbac/assigments/edit')
        ]);
    }
}
