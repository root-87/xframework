<?php

namespace Src\Traits;

use Framework\App;
use Src\Common\FC\MailSender\Mail;
use Src\Common\FC\MailSender\Send\Smtp;


/**
 * Description of MailSender
 *
 * @author root87x
*/

trait MailSender
{
    public function smtp(string $subject, $to, string $from, string $body = '')
    {
        $smtp_conf = App::I()->conf('app.mailsender.smtp');
        
        if (!$smtp_conf) {
            throw new \Exception('Не найдены настройки почтового сервера');
        }
        
        $smtp = new Smtp($smtp_conf['debug']);
        $smtp->setHost($smtp_conf['host']);
        $smtp->setUsername($smtp_conf['username']);
        $smtp->setPassword($smtp_conf['password']);
        $smtp->setSecure($smtp_conf['secure']);
        $smtp->setPort($smtp_conf['port']);
        
        $mail = new Mail($smtp);
        $mail->subject($subject);
        $mail->isHtml(true);
        $mail->addAddress($to);
        $mail->from($from);
        $mail->body($body);
        
        return $mail;
    }
}
