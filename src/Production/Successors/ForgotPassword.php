<?php

namespace Src\Production\Successors;

use Framework\Storage\Session\Session;
use Framework\Http\{
    Request, 
    Response    
};
use Framework\Patterns\ChainOfResponsibility;
use Src\Production\Models\ForgotPassword AS ModelForgotPassword;

/**
 * Description of ForgotSendmail
 *
 * @author root87x
*/

class ForgotPassword extends ChainOfResponsibility
{
    /** @var Session */
    private $session;
    
    public function __construct(array $data, ChainOfResponsibility $successor = null)
    {
        $this->data = $data;
        
        parent::__construct($successor);
    }
    
    protected function processing(Request $request, Response $response): bool
    {
        $forgot = new ModelForgotPassword();
        $user_id = $this->data['user_id'];
        
        $forgot->sign = $this->data['sign'];
        $forgot->user_id = $user_id;
        $forgot->dateto = date('Y-m-d H:i:s', time() + 3600 * 12);
        
        return $forgot->save();
    }
}
