<?php

namespace Src\Production\Successors;

use Framework\App;
use Framework\Http\{Url, Request, Response};
use Framework\Patterns\ChainOfResponsibility;
use Framework\Views\ViewAdapter;

/**
 * Description of ForgotMailSend
 *
 * @author root87x
 */
class ForgotMailSend extends ChainOfResponsibility
{
    use \Src\Traits\MailSender;
    
    /** @var array */
    private $data;
    
    /** @var ViewAdapter */
    private $view;
    
    public function __construct(array $data, ChainOfResponsibility $successor = null)
    {
        $this->data = $data;
        $this->view = App::I()->getContainer()->get('example')->getView();
        
        parent::__construct($successor);
    }
    
    protected function processing(Request $request, Response $response)
    {
        $host = $request->getHost();

        ob_start();
            $this->view->render('/forgotpassword/email', [
                'host' => $host,
                'url' => $host . Url::to('/forgot-password/confirm/', [
                    'sign' => $this->data['sign']
                ])
            ]);
            $content = ob_get_contents();
        ob_clean();    

        $this->smtp($this->data['subject'], $this->data['to'], $this->data['from'], $content);
        
        if (!$smtp->send()) {
            return false;
        }
        
        return null;
    }
}
