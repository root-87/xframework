<?php

namespace Src\Production;

use Framework\Views\ViewAdapter;
use Framework\Views\Templates\Twig;

/**
 * Description of Production
 *
 * @author root87x
 */
class Production
{
    const VIEW = __DIR__ . '/Views';
    
    public function getView()
    {
        $view = new ViewAdapter(new Twig());
        $view->setViewPath(self::VIEW);
        $view->setLayoutPath(self::VIEW . '/Layouts');
        
        $view->registerObject('url', \Framework\Http\Url::class);
        $view->registerObject('auth', \Src\Common\Helpers\Auth::class);
        
        return $view;
    }
}
