<?php

namespace Src\Production\Controllers;

use Framework\Views\ViewAdapter;

/**
 * Abstract Controller of App
 * @abstract
 * 
 * @author root87x
 * @version 1.1.6
*/

abstract 
class Controller implements \Framework\Psr\Controller
{
    /**
     * Адаптер view
     * @var ViewAdapter 
    */
    protected $view; 
           
    /**
     * Инициализация контроллера
     * @param \Framework\Storage\DIContainer $container
    */
    public function __construct(\Framework\Storage\DIContainer $container)
    {
        $this->view = $container->get('example')->getView();
    }
    
    /**
     * Отобразить шаблон
     * 
     * @param string $view
     * @param array $data
     * @return type
    */
    protected function render(string $view, array $data = [])
    {
        return $this->view->render($view, $data);
    }
    
    /**
     * Буфферезировать шаблон
     * 
     * @param string $view
     * @param array $data
     * @return type
    */
    protected function buffer(string $view, array $data = [])
    {
        ob_start();
            $content = $this->render($view, $data);
        ob_get_contents();
        
        return $content;
    }
}
