<?php

namespace Src\Production\Controllers;

use Framework\Http\{
    Request, 
    Response    
};

/**
 * Index page
 *
 * @author root87x
*/
class SiteController extends Controller
{
    public function beforeAction() 
    {
        $this->view->addVars([
            'title' => 'xFramework'
        ]);
    }
    
    public function view(Request $request, Response $response)
    {    
        return $this->render('/Site/Index');
    }
}
