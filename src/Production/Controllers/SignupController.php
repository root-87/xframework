<?php

namespace Src\Production\Controllers;

use Src\Common\Models\SignupModel;

use Framework\Http\{
    Url,
    Response, 
    Request    
};

/**
 * Класс регистрации
 *
 * @author root87x
 */
class SignupController extends Controller
{
    /** @var SignupModel */
    private $model;
    
    /** @var bool */
    private $register;
    
    public function beforeAction()
    {
        $this->view->addVars([
            'title' => 'Регистрация',
            'description' => '',
            'keywords' => ''
        ]);
        
        $this->model = new SignupModel();
    }
    
    /**
     * Регистрация 
     * 
     * @param Request $request
     * @param Response $response
     * @return type
    */
    public function reg(Request $request, Response $response)
    {
        $this->model->setAttributes($request->getParams());
        
        if ($this->model->validation()) {
            $this->model->setPassword($this->model->password);
            $this->register = $this->model->save();
        } 
                
        $redirect = Url::to('/signup', [
            'signup' => [
                'login' => $this->model->login,
                'email' => $this->model->email
            ],
            'alert' => [
                'type' => 'danger',
                'body' => 'Не удалось зарегистрировать пользователя'
            ],
            'errors' => $this->model->errors()
        ]);    
        
        if ($this->register) {
            $redirect = Url::to('/auth', [
                'alert' => [
                    'type' => 'success',
                    'body' => 'Пользователь успешно зарегистрирован'
                ]
            ]);
        }
        
        return $request->redirect()->go302($redirect);
    }
    
    public function view(Request $request, Response $response)
    {
        $signup_errors = $request->getParam('errors', []);
        $signup_query = $request->getParam('signup', []);
        
        $this->model->setAttributes($signup_query);
        
        return $this->render('/Signup/index', [
            'model' => $this->model, 
            'errors' => $signup_errors
        ]);
    }
}
