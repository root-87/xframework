<?php

namespace Src\Production\Controllers;

use Src\Common\Models\Users;
use Src\Production\Models\ForgotPassword AS ModelForgotPassword;

use Src\Production\Successors\{
    ForgotPassword, 
    ForgotMailSend
};

use Framework\Http\{
    Response, 
    Request    
};

use Framework\Str;

/**
 * Восстановление пароля
 *
 * @author root87x
 */
class ForgotPasswordController extends Controller
{
    use \Src\Traits\MailSender;
    
    /** @var ModelForgotPassword */
    private $model;
    
    private $sign = null;
    
    /** @var bool */
    private $forgot = false;

    public function beforeAction() 
    {
        $this->sign = sha1(md5(Str::genString()));
        $this->model = new ModelForgotPassword();
        
        $this->view->addVars([
            'title' => 'Восстановление пароля'
        ]);
    }
    
    public function request(Request $request, Response $response) 
    {
        $errors = [];
        $alert = [
            'type' => 'danger',
            'body' => ModelForgotPassword::SEND_MAIL_FAILED
        ];

        $this->model->setAttributes($request->getParams());
        $user = Users::where('login', $this->model->login)->first();
        
        if (!$user) {
            $errors[] = 'Пользователь ' . $this->model->login . ' не найден';
        }
        
        if ($this->model->validation()) {
            $this->forgot = (new ForgotMailSend([
                'subject' => 'Восстановление пароля',
                'to' => $user->email,
                'from' => 'robot@localhost',
                'sign' => $this->sign
            ], new ForgotPassword([
                'user_id' => $user->id,
                'sign' => $this->sign
            ], null)))->handler($request, $response);
        }
        
        if ($this->forgot) {
            $alert = [
                'type' => 'success', 
                'body' => ModelForgotPassword::SEND_MAIL_SUCCESS
            ];
        }
        
        return $request->redirect()->go301(Url::to('/forgotpassword', [
            'alert' => $alert,
            'errors' => $this->model->errors() + $errors
        ]));
    }
    
    public function view(Request $request, Response $response): ?string
    {
        $alert = $request->getParam('alert', []);
        $errors = $request->getParam('errors', []);
        
        return $this->render('/ForgotPassword/Index', [
            'model' => $this->model,
            'alert' => $alert,
            'errors' => $errors
        ]);
    }

}
