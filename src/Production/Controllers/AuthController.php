<?php

namespace Src\Production\Controllers;

use Framework\Http\{
    Url,
    Request,
    Response
};
use Src\Common\FC\Auth;
use Src\Common\Models\LoginModel;

/**
 * Description of AuthController
 *
 * @author root87x
 */
class AuthController extends Controller
{
    /** @var Auth */
    private $auth;
    
    /** @var LoginModel */
    private $model;
    
    /** @var bool */
    private $login = false;    
    
    public function beforeAction()
    {
        $this->view->addVars([
            'title' => 'Авторизоваться',
            'description' => '',
            'keywords' => ''
        ]);
        
        $this->auth = new Auth();
        $this->model = new LoginModel();
    }

    /**
     * Раз авторизовать
     * 
     * @param Request $request
     * @param Response $response
     * @return type
     */
    public function logout(Request $request, Response $response)
    {
        $this->auth->logout();
        return $request->redirect()->go302(Url::to('/'));
    }

    /**
     * Залогинить
     * @param Request $request
     * @param Response $response
     */
    public function login(Request $request, Response $response)
    {
        $this->model->setAttributes($request->getParams());
        
        if ($this->model->validation()) {
            $this->login = $this->auth->login(
                $this->model->login, 
                $this->model->password, 
                (bool)$this->model->remember_me
            );
        }
        
        $redirect = $this->login ? Url::to('/') : Url::to('/auth', [
            'auth' => [
                'login' => $this->model->login
            ],
            'alert' => [
                'type' => 'danger',
                'body' => 'Неверный логин или пароль'
            ],
            'errors' => $this->model->errors()
        ]);
        
        $request->redirect()->go302($redirect);
    }

    /**
     * Страница
     * 
     * @param Request $request
     * @param Response $response
     * @return type
     */
    public function view(Request $request, Response $response)
    {
        $alert_query = $request->getParam('alert', []);
        $auth_query = $request->getParam('auth', []);
        $auth_error = $request->getParam('errors', []);
        
        $this->model->setAttributes($auth_query);
        
        return $this->render('/Auth/Index', [
            'csrf' => $this->model->csrfoken(),
            'alert' => $alert_query,
            'errors' => $auth_error,
            'model' => $this->model
        ]);
    }

}
