<?php

namespace Src\Production\Models;

/**
 * Description of ForgotPassword
 *
 * @author root87x
*/

class ForgotPassword extends \Src\Common\Models\Users 
{
    protected $table = 'forgotpassword';
    
    public const SEND_MAIL_SUCCESS = 'На вашу почту отправлено письмо с подтверждением';
    public const SEND_MAIL_FAILED = 'Не удалось отправить письмо с подтверждением';
    
    protected function rules() 
    {
        return [
            'lengthMin' => [
                ['login', 4]
            ],
        ];
    }
}
